﻿using System;

class StringProgram
{
    public static void StringDemo()
    {
        // Creating a string
        string greeting = "Hello, World!";

        // Getting the length of the string
        int length = greeting.Length;
        Console.WriteLine($"Length: {length}");

        // Concatenation
        string firstName = "John";
        string lastName = "Doe";
        string fullName = firstName + " " + lastName;

        // String interpolation
        string formattedGreeting = $"{greeting}, {fullName}";
        Console.WriteLine(formattedGreeting);

        // Substring
        string substring = greeting.Substring(0, 5);
        Console.WriteLine("Demo Substring: ");
        Console.WriteLine(substring);
        
        //Split
        string names = "John,Jane,Jim";
        string[] nameArray = names.Split(','); // ["John", "Jane", "Jim"]
        Console.WriteLine("Demo Split: ");

        foreach ( string name in nameArray )
        {
            Console.WriteLine(name);
        }

        //Replace
        string message = "Hello, World!";
        string newMessage = message.Replace("World", "Universe"); // "Hello, Universe!"
        Console.WriteLine("Demo Replace: " + newMessage);

        //Trim
        string paddedString = "   Trim me!   ";
        string trimmedString = paddedString.Trim(); // "Trim me!"
        Console.WriteLine("Demo Trim: " + trimmedString);

        //Join an array elements
        string[] words = { "Hello", "World", "C#" };
        string sentence = string.Join(" ", words, " "); // "Hello World C# "
        Console.WriteLine("Demo Join: " + sentence);


        // Accessing individual characters
        char firstChar = greeting[0];
        Console.WriteLine($"First character: {firstChar}");

        // String comparison
        string str1 = "abc";
        string str2 = "ABC";
        bool areEqual = str1.Equals(str2, StringComparison.OrdinalIgnoreCase);
        Console.WriteLine($"Are equal: {areEqual}");

        // ToUpper and ToLower
        string upperCaseGreeting = greeting.ToUpper();
        string lowerCaseGreeting = greeting.ToLower();

        // Checking if a string is null or empty
        string emptyString = "";
        bool isNullOrEmpty = string.IsNullOrEmpty(emptyString);

        Console.WriteLine($"IsNullOrEmpty: {isNullOrEmpty}");
    }
}
