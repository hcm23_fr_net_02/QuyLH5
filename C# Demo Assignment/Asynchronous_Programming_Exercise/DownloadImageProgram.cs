﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Reflection;
using System.Threading.Tasks;
using static System.Net.Mime.MediaTypeNames;

class Program
{
    static async Task Main(string[] args)
    {
        List<string> imageUrls = new List<string>
        {
            "https://fastly.picsum.photos/id/803/200/300.jpg?hmac=v-3AsAcUOG4kCDsLMlWF9_3Xa2DTODGyKLggZNvReno",
            "https://fastly.picsum.photos/id/1021/200/300.jpg?hmac=hTRmKnWJoXfl5p7_LZK9lWOLXJZiN6_EUJmWpmZIXYI",
            "https://fastly.picsum.photos/id/1002/200/300.jpg?hmac=QAnT71VGihaxEf_iyet9i7yb3JvYTzeojsx-djd3Aos",

            // Thêm các URL hình ảnh khác vào đây
        };

        string directoryPath = "C:\\Downloads";

        Directory.CreateDirectory(directoryPath);

        await DownloadImagesAsync(imageUrls, directoryPath);

        Console.WriteLine("Done");
    }

    static async Task DownloadImagesAsync(List<string> urls, string directory)
    {
            HttpClient httpClient = new HttpClient();


        List<Task> downloadTasks = new List<Task>();
            //Create a list of download Task
            foreach (string url in urls)
            {
                downloadTasks.Add(DownloadImageAsync(httpClient,url,directory));
            }
            // Then WhenAll the download Task
            await Task.WhenAll(downloadTasks);
        
    }

    static async Task DownloadImageAsync(HttpClient httpClient,string url,string filePath)
    {
        try
        {
            var responseResult = await httpClient.GetAsync(url);
            responseResult.EnsureSuccessStatusCode();
            using (var memStream = responseResult.Content.ReadAsStreamAsync().Result)
            {
                var fileName = 
                responseResult.Content.Headers.ContentDisposition.FileName.Substring(1, responseResult.Content.Headers.ContentDisposition.FileName.Length - 2);
                using (var fileStream = File.Create(filePath + "\\" +  fileName))
                {
                    memStream.CopyTo(fileStream);
                }
                Console.WriteLine($"Downloaded {fileName} at " + filePath + "\\" + fileName);


            }

        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error downloading {url}: {ex.Message}");
        }
    }
}
