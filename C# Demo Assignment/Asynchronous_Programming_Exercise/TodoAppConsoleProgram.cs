﻿using Newtonsoft.Json;
using System.Net.Http.Json;

class Program
{
    public class TodoItem
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool IsCompleted { get; set; }
        public int Priority { get; set; }
        public DateTime CreatedDate { get; set; }
        // Các thuộc tính khác tùy theo yêu cầu

        // Constructor để khởi tạo một TodoItem
        public TodoItem(string name, bool isCompleted, int priority, DateTime createdDate)
        {
            Name = name;
            IsCompleted = isCompleted;
            Priority = priority;
            CreatedDate = createdDate;
        }

        public void Print()
        {
            Console.WriteLine("Id:" + Id);
            Console.WriteLine("Name: " + Name);
            Console.WriteLine("IsCompleted: " + IsCompleted);
            Console.WriteLine("Priority: " + Priority);
            Console.WriteLine("CreatedDate: " + CreatedDate);
        }
    }

    static async Task Main(string[] args)
    {

        while (true)
        {
            try
            {
                Console.WriteLine("Chọn chức năng:");
                Console.WriteLine("0. Hiển thị danh sách các todo");
                Console.WriteLine("1. Hiển thị todo với Id");
                Console.WriteLine("2. Thêm một todo mới");
                Console.WriteLine("3. Xoá một todo");
                Console.WriteLine("4. Đánh dấu một todo là đã hoàn thành");
                Console.WriteLine("5. Nâng độ ưu tiên");
                Console.WriteLine("6. Thoát ứng dụng");

                int choice = int.Parse(Console.ReadLine());

                switch (choice)
                {
                    case 0:
                        await DisplayTodos();
                        break;
                    case 1:
                        await DisplayTodoWithId();
                        break;
                    case 2:
                        await AddNewTodo();
                        break;
                    case 3:
                        await DeleteTodo();
                        break;
                    case 4:
                        await MarkTodoAsCompleted();
                        break;
                    case 5:
                        await IncreasePriority();
                        break;
                    case 6:
                        Environment.Exit(0);
                        break;
                  
                    default:
                        Console.WriteLine("Chọn không hợp lệ. Vui lòng chọn lại.");
                        break;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Bug was detected: " + ex.ToString());
            }

        }
    }
    

    static async Task DisplayTodos()
    {
        HttpClient client = new HttpClient(); // Create HttpClientObject
        HttpResponseMessage response = await client.GetAsync("https://651e32d644e393af2d5a96dc.mockapi.io/api/v1/TodoItem"); // Call mockApi url and store in HttpResponseMessage object
        response.EnsureSuccessStatusCode(); // Throw an exeption if status code isnt a success one
        string responseBody = await response.Content.ReadAsStringAsync(); // parse response into string
        var todoItems = JsonConvert.DeserializeObject<List<TodoItem>>(responseBody); // Parse string into list todoitem using DeserializeObject of newtonJson
        if (todoItems != null)
        {
            foreach (TodoItem item in todoItems)
            {
                item.Print();
            }
        }
        else
            Console.WriteLine("respone trong!");
    }
    static async Task DisplayTodoWithId()
    {
        HttpClient client = new HttpClient(); // Create HttpClientObject

        Console.Write("Nhap id: ");
        string id = Console.ReadLine() ?? "";
        HttpResponseMessage response = await client.GetAsync("https://651e32d644e393af2d5a96dc.mockapi.io/api/v1/TodoItem/" + id); // Call mockApi url and store in HttpResponseMessage object
        if (!response.IsSuccessStatusCode)
        {
            Console.WriteLine("Khong ton tai todoItem");
            return;
        }
        //response.EnsureSuccessStatusCode(); // Throw an exeption if status code isnt a success one
        string responseBody = await response.Content.ReadAsStringAsync(); // parse response into string
        var todoItem = JsonConvert.DeserializeObject<TodoItem>(responseBody); // Parse string into list todoitem using DeserializeObject of newtonJson
        if (todoItem != null)
        {
            todoItem.Print();
        }
        else
            Console.WriteLine("respone trong!");
    }

    static async Task AddNewTodo()
    {
        Console.WriteLine("Nhập thông tin cho todo mới");
        Console.Write("Nội dung công việc: ");
        string content = Console.ReadLine() ?? "";

        Console.Write("Độ ưu tiên: ");
        int priority = int.Parse(Console.ReadLine() ?? "0");

        HttpClient client = new HttpClient();
        var content1 = new TodoItem(
            name: content,
            isCompleted: false,
            priority: priority,
            createdDate: DateTime.Now
            );

        HttpResponseMessage response = await client.PostAsJsonAsync("https://651e32d644e393af2d5a96dc.mockapi.io/api/v1/TodoItem", content1);
        response.EnsureSuccessStatusCode();
        Console.WriteLine("Todo mới đã được thêm.");
    }

   

    //TODO: nang cap
    static async Task IncreasePriority()
    {
        Console.WriteLine("Tang priority cho todo");
        Console.Write("Nhap id: ");
        string id = Console.ReadLine() ?? "";

        
        HttpClient client = new HttpClient();
        var response = await client.GetAsync("https://651e32d644e393af2d5a96dc.mockapi.io/api/v1/TodoItem/" + id);
        response.EnsureSuccessStatusCode();

        Console.Write("Độ ưu tiên: ");
        int priority = int.Parse(Console.ReadLine() ?? "0");

        var responseAsString = await response.Content.ReadAsStringAsync();
        var todoOjbect = JsonConvert.DeserializeObject<TodoItem>(responseAsString);
        if (todoOjbect is not null)
        {
            todoOjbect.Priority = priority;

            var updateResponse = await client.PutAsJsonAsync("https://651e32d644e393af2d5a96dc.mockapi.io/api/v1/TodoItem/" + todoOjbect.Id, todoOjbect);
            updateResponse.EnsureSuccessStatusCode();

            Console.WriteLine("Cập nhật todoitem với id: " + todoOjbect.Id);
        }
        else
        {
            throw new Exception("TodoItem null");
        }
    }

    static async Task DeleteTodo()
    {
        Console.WriteLine("Xoa todo");
        Console.Write("Nhap id: ");
        string id = Console.ReadLine() ?? "";
       
        HttpClient client = new HttpClient();
        var response = await client.DeleteAsync("https://651e32d644e393af2d5a96dc.mockapi.io/api/v1/TodoItem/" + id);
        response.EnsureSuccessStatusCode();

        Console.WriteLine("Xoa thanh cong");
      
    }

    static async Task MarkTodoAsCompleted()
    {
        Console.WriteLine("Da hoan thanh todo");
        Console.Write("Nhap id: ");
        string id = Console.ReadLine() ?? "";
        
        HttpClient client = new HttpClient();
        var response = await client.GetAsync("https://651e32d644e393af2d5a96dc.mockapi.io/api/v1/TodoItem/" + id);
        response.EnsureSuccessStatusCode();

        var responseAsString = await response.Content.ReadAsStringAsync();
        var todoOjbect = JsonConvert.DeserializeObject<TodoItem>(responseAsString);
        if (todoOjbect is not null)
        {
            todoOjbect.IsCompleted = true;

            var updateResponse = await client.PutAsJsonAsync("https://651e32d644e393af2d5a96dc.mockapi.io/api/v1/TodoItem/" + todoOjbect.Id, todoOjbect);
            updateResponse.EnsureSuccessStatusCode();

            Console.WriteLine("Cập nhật todoitem hoan thanh với id: " + todoOjbect.Id);
        }
        else
        {
            throw new Exception("TodoItem null");
        }
    }
}