﻿using System;

static class StackProgram
{

    public class StackNode<T> where T :  notnull
    {
        public T Value { get; set; }
        public StackNode<T>? Next { get; set; }

        public StackNode(T value)
        {
            Value = value;
        }
    }

    public class Stack<T> where T : notnull
    {
        private StackNode<T>? head;
        private int count;

        public Stack()
        {
            head = null;
            count = 0;
        }

        public void Push(T value)
        {
            StackNode<T> newNode = new StackNode<T>(value);
            newNode.Next = head;
            head = newNode;
            count++;
        }

        public T Pop()
        {
            if (head == null)
            {
                throw new Exception("The stack is empty.");
            }

            T value = head.Value;
            head = head.Next;
            count--;
            return value;
        }

        public T Peek()
        {
            if (head == null)
            {
                throw new Exception("The stack is empty.");
            }

            return head.Value;
        }

        public int Count
        {
            get { return count; }
        }

        public bool IsEmpty
        {
            get { return head == null; }
        }
    }


    public static void StackDemo()
    {
        Stack<int> stack = new Stack<int>();

        stack.Push(1);
        stack.Push(2);
        stack.Push(3);

        Console.WriteLine(stack.Peek()); // Outputs "3"
        Console.WriteLine(stack.Pop()); // Outputs "3"
        Console.WriteLine(stack.Pop()); // Outputs "2"


    }



}
