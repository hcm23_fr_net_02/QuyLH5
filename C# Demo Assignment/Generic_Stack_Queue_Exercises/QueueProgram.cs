﻿using System;

static class QueueProgram
{

    public class QueueNode<T> where T : notnull
    {
        public T Value { get; set; }
        public QueueNode<T>? Next { get; set; }

        public QueueNode(T value)
        {
            Value = value;
        }
    }


    public class Queue<T> where T : notnull
    {
        private QueueNode<T>? head;
        private QueueNode<T>? tail;
        private int count;

        public Queue()
        {
            head = null;
            tail = null;
            count = 0;
        }

        public void Enqueue(T value)
        {
            QueueNode<T> newNode = new QueueNode<T>(value);
            if (head == null)
            {
                head = newNode;
                tail = newNode;
            }
            else
            {
                tail.Next = newNode;
                tail = newNode;
            }
            count++;
        }

        public T Dequeue()
        {
            if (head == null)
            {
                throw new Exception("The queue is empty.");
            }

            T value = head.Value;
            head = head.Next;
            count--;
            return value;
        }

        public T Peek()
        {
            if (head == null)
            {
                throw new Exception("The queue is empty.");
            }

            return head.Value;
        }

        public int Count
        {
            get { return count; }
        }

        public bool IsEmpty
        {
            get { return head == null; }
        }
    }
    public static void QueueDemo()
    {
        Queue<int> queue = new Queue<int>();

        queue.Enqueue(1);
        queue.Enqueue(2);
        queue.Enqueue(3);

        Console.WriteLine(queue.Dequeue()); // Outputs "1"
        Console.WriteLine(queue.Dequeue()); // Outputs "2"
        Console.WriteLine(queue.Dequeue()); // Outputs "3"


    }



}
