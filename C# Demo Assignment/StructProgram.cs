﻿using System;

class StructProgram
{
    /*
     Answer these questions:
        - Stack Allocation, Stack Allocation
        - Performance, Used for Small Data
        - No Inheritance
        - Structs can implement interfaces.
        - structs in C# cannot have a default constructor. This is because structs are value types, and value types are not required to have a constructor.
        - Structs are a good choice for representing small pieces of data that are immutable
     */
    struct Point
    {
        public int X;
        public int Y;

        public Point(int x, int y)
        {
            X = x;
            Y = y;
        }
    }
    public static void StructDemo()
    {

        Point p1 = new Point(10, 20);
        Point p2 = p1; // p2 is a copy of p1

        p2.X = 30; // Changes p2, but not p1

        Console.WriteLine($"p1: ({p1.X}, {p1.Y})"); // Output: p1: (10, 20)
        Console.WriteLine($"p2: ({p2.X}, {p2.Y})"); // Output: p2: (30, 20)

    }

}

