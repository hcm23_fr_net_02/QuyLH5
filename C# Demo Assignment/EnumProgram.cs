﻿using System;

static class EnumProgram
{
    /*
     
        Here are some of the features of enums in C#:

        Enums are value types. This means that when you assign an enum variable to another variable, a copy of the enum value is created.
        Enums can contain named constants.
        Enums can have an underlying type. The underlying type can be byte, sbyte, short, ushort, int, uint, long, or ulong.
        Enums can be flagged enums. Flagged enums allow you to combine multiple enum values into a single value.
        Enums can be used as members of other enums.
     */
    public static T ToEnum<T>(this string value)
    {
        return (T)Enum.Parse(typeof(T), value, true);
    }

    public static T ToEnum<T>(this string value, T defaultValue) where T : struct
    {
        if (string.IsNullOrEmpty(value))
        {
            return defaultValue;
        }

        T result;
        return Enum.TryParse<T>(value, true, out result) ? result : defaultValue;
    }

    public enum DaysOfWeek
    {
        Sunday,
        Monday,
        Tuesday,
        Wednesday,
        Thursday,
        Friday,
        Saturday,
        Unknown
    }
    public static void EnumDemo()
    {
        // Creating a string
        DaysOfWeek dayOfWeek = DaysOfWeek.Monday;
        int dayOfWeekValue = (int)dayOfWeek;

        var sundayString = "Sunday";
        DaysOfWeek toEnumTest = sundayString.ToEnum<DaysOfWeek>();
        Console.WriteLine(toEnumTest);


        var sundayStringButWrongType = "SundayUwU";
        DaysOfWeek toEnumTestButWrong = sundayStringButWrongType.ToEnum<DaysOfWeek>(DaysOfWeek.Sunday);

        Console.WriteLine(toEnumTestButWrong);

    }



}
