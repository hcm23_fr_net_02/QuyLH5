﻿using System;

static class DateTimeProgram
{
 
    
    public static void DateTimeDemo()
    {
        // Create a new instance of the DateTime structure.
        DateTime now = DateTime.Now;

        // Get the current date.
        DateTime today = now.Date;

        // Get the current time.
        TimeSpan currentTime = now.TimeOfDay;
        Console.WriteLine(currentTime);

        // Add one day to the current date.
        DateTime tomorrow = today.AddDays(1);

        // Subtract one month from the current date.
        DateTime lastMonth = today.AddMonths(-1);

        // Compare two dates and times.
        bool isBefore = today < tomorrow;

        // Format a date and time into a stringy
        string formattedDate = now.ToString("dd MM yyyy"); // Outputs "10/03/2023"
        //Format details: https://learn.microsoft.com/en-us/dotnet/standard/base-types/custom-date-and-time-format-strings
        Console.WriteLine("Demo Format:" + formattedDate);

        // Create two DateTime objects.
        DateTime startDate = new DateTime(2023, 10, 3, 12, 0, 0);
        DateTime endDate = new DateTime(2023, 10, 3, 13, 0, 0);

        // Calculate the time difference between the two dates and times.
        TimeSpan timeSpan = endDate - startDate;

        // Get the hours, minutes, and seconds of the time span.
        int hours = timeSpan.Hours;
        int minutes = timeSpan.Minutes;
        int seconds = timeSpan.Seconds;

        // Display the time difference.
        Console.WriteLine($"The time difference is {hours}:{minutes}:{seconds}.");
    }



}
