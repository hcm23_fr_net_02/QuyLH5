var fruits = new List<string> {
"apple",
"banana",
"orange",
"pineapple",
"mango"
};

//Chọn và in tên các trái cây trong mảng dưới dạng chữ hoa.
foreach(var fruit in fruits)
{
    Console.WriteLine(fruit.ToUpper());
}
//2. Chọn và in tên các trái cây trong mảng cùng với độ dài của chúng. Ví dụ: "apple:5", "banana:6", "orange:6", "pineapple:9", "mango:5".
foreach (var fruit in fruits)
{
    Console.WriteLine(fruit + ": " + fruit.Length);
}
//3.Chọn và in kí tự đầu tiên của các trái cây trong mảng.
foreach (var fruit in fruits)
{
    Console.WriteLine(fruit.ElementAt(0));
}
//4. Chọn và in tên các trái cây trong mảng theo chiều đảo ngược.
foreach (var fruit in fruits)
{
    Console.WriteLine(new string(fruit.Reverse().ToArray()));
}

//1.Chọn và in ra các trái cây có độ dài là 6 ký tự.
fruits.Where(x => x.Length > 6).ToList().ForEach(x => Console.WriteLine(x + " has length > 6chars"));

//2. Chọn và in ra các trái cây bắt đầu bằng "a".
fruits.Where(x => x.StartsWith('a')).ToList().ForEach(x => Console.WriteLine(x + " has length > 6chars"));
//3. Chọn và in ra các trái cây chứa chữ "a".
fruits.Where(x => x.Contains('a')).ToList().ForEach(x => Console.WriteLine(x + " has length > 6chars"));