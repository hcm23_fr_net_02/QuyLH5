List<int> numbers = Enumerable.Range(0, 50).ToList();
//Cài đặt số phần tử trên một trang (mặc định là 10 phần tử).
int pageIndex = 1;
int pageSize = 10;
int totalPage = numbers.Count / pageSize;
while (true)
{
    // Hiển thị menu
    Console.WriteLine("Chọn chức năng:");
    Console.WriteLine("1. Cài đặt số phần tử trên một trang");
    Console.WriteLine("2. Xem danh sách các số nguyên");
    Console.WriteLine("3. Tìm kiếm số nguyên");
    Console.WriteLine("4. Thoát");
    Console.Write("Option: ");

    // Nhập lựa chọn của người dùng
    int choice = Convert.ToInt32(Console.ReadLine());

    // Thực hiện lựa chọn của người dùng
    switch (choice)
    {
        case 1:
            // Hiển thị số phần tử trên một trang hiện tại
            Q1();

            break;

        case 2:
            // Xem danh sách các số nguyên
            if (pageIndex == totalPage)
            {
                Console.WriteLine("Lựa chọn không hợp lệ!");

            }
            else
            {
                pageIndex++;
                Q1();
            }

            break;

        case 3:
            if (pageIndex == totalPage)
            {
                Console.WriteLine("Lựa chọn không hợp lệ!");
            }
            else
            {
                pageIndex--;
                Q1();
            }
            break;

        case 4:
            // Thoát khỏi chương trình
            return;

        default:
            // Lựa chọn không hợp lệ
            Console.WriteLine("Lựa chọn không hợp lệ!");
            break;
    }

}

void Q1()
{
    numbers.Skip(pageSize * (pageIndex - 1)).Take(pageSize).ToList().ForEach(x => Console.WriteLine(x));
    if (pageIndex == totalPage)
    {
        Console.WriteLine("Bam 3 de ve trang phia truoc");
    }
    else if (pageIndex == 0)
    {
        Console.WriteLine("Bam 2 de chon trang tiep theo");
    }
    else
    {
        Console.WriteLine("Bam 2 de chon trang tiep theo");
        Console.WriteLine("Bam 3 de ve trang phia truoc");
    }
}