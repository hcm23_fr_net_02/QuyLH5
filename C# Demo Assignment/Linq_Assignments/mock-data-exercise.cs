
// đọc file mock-data.json trong project để lấy dữ liệu
var filePath = "mock-data.json"; // Provide the path to your JSON file
List<Employee>? employees = null;
if (File.Exists(filePath))
{
    string jsonContent = File.ReadAllText(filePath);
     employees = JsonConvert.DeserializeObject<List<Employee>>(jsonContent);

}
else
{
    Console.WriteLine("File not found.");
}
if (employees == null) return;

// sử dụng LinQ để lọc các dữ liệu như yêu cầu bên dưới
foreach (Employee employee in employees)
{
    Console.WriteLine($"ID: {employee.Id}, Name: {employee.FirstName} {employee.LastName}, Email: {employee.Email}");
}
//Lấy ra danh sách các FirstName của tất cả các nhân viên.
employees.Select(x => x.FirstName).ToList().ForEach(x => Console.WriteLine(x));

//Lấy ra danh sách các nhân viên có Salary lớn hơn 50000$.
employees
    .Where(x => x.Salary > 50000).
    ToList()
    .ForEach(x => x.Print());


//Lấy ra danh sách các nhân viên có Gender là "Male" và sắp xếp tăng dần theo FirstName.
employees
    .Where(x => x.Gender == "Male")
    .OrderBy( x=> x.FirstName)
    .ToList().
    ForEach(x => x.Print());


//Lấy ra danh sách các nhân viên có Country là "Indonesia" và JobTitle chứa "Manager".
employees
    .Where(x => x.Country == "Indonesia" && x.JobTitle == "Manager")
    .ToList()
    .ForEach(x => x.Print());


//Lấy ra danh sách các nhân viên có Email và sắp xếp giảm dần theo LastName.
employees
    .Where(x => x.Email != null)
    .OrderByDescending(x => x.LastName)
    .ToList()
    .ForEach(x => x.Print());

//Lấy ra danh sách các nhân viên có StartDate trước ngày "2022-01-01" và Salary lớn hơn 60000$.

employees
    .Where(x => x.StartDate < DateTime.Parse("2022-01-01") && x.Salary > 60000)
    .ToList()
    .ForEach(x => x.Print());
//Lấy ra danh sách các nhân viên có PostalCode là null hoặc rỗng và sắp xếp tăng dần theo LastName.
employees
    .Where(x => x.PostalCode == null && x.Salary > 60000)
    .ToList()
    .ForEach(x => x.Print());
//Lấy ra danh sách các nhân viên có FirstName và LastName được viết hoa và sắp xếp giảm dần theo Id.
employees
    .OrderByDescending(x => x.Id)
    .Select(x => $"{x.FirstName.ToUpper()} {x.LastName.ToUpper()} ")
    .ToList()
    .ForEach(x => Console.WriteLine(x));
//Lấy ra danh sách các nhân viên có Salary nằm trong khoảng từ 50000$ đến 70000$ và sắp xếp tăng dần theo Salary.
employees
    .Where(x => x.Salary > 50000 && x.Salary < 70000)
    .ToList()
    .ForEach(x => x.Print());
//Lấy ra danh sách các nhân viên có FirstName chứa chữ "a" hoặc "A" và sắp xếp giảm dần theo LastName.
employees
    .Where(x => x.FirstName.Any(c => c == 'a' || c == 'A') )
    .OrderByDescending(x => x.LastName)
    .ToList()
    .ForEach(x => x.Print());
public class Employee
{
    public int Id { get; set; }
    public string FirstName { get; set; }
    public string LastName { get; set; } // Note: Corrected property name to "LastName"
    public string Email { get; set; }
    public string Gender { get; set; }
    public string Country { get; set; } // Note: Changed property name to "Country"
    public string PostalCode { get; set; }
    public string JobTitle { get; set; }
    public DateTime? StartDate { get; set; }
    public decimal Salary { get; set; }

    public void Print()
    {
        Console.WriteLine($"ID: {this.Id}, Name: {this.FirstName} {this.LastName}, Email: {this.Email}");
    }
}