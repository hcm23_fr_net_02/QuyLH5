//Bài tập kết hợp
List<int> numbers = new List<int> { 3, 8, 1, 5, 2, 10, 7 };
// a) Lấy ra các số chẵn và sắp xếp tăng dần kq: 2 8 10
numbers.Where(x => x % 2 == 0).OrderBy(x => x).ToList().ForEach(x => Console.WriteLine(x));
// b) Lấy ra các số lẻ và sắp xếp giảm dần kq: 7 5 3 1
numbers.Where(x => x % 2 != 0).OrderByDescending(x => x).ToList().ForEach(x => Console.WriteLine(x));
// c) Lấy ra các số lớn hơn 5 và nhỏ hơn 10 và sắp xếp 
numbers.Where(x => x > 5 && x < 10).OrderBy(x => x).ToList().ForEach(x => Console.WriteLine(x));