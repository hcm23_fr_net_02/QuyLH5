var filePath = "MOCK_DATA.json"; // Provide the path to your JSON file
List<Order>? orders = null;
if (File.Exists(filePath))
{
    string jsonContent = File.ReadAllText(filePath);
    orders = JsonConvert.DeserializeObject<List<Order>>(jsonContent);

}
else
{
    Console.WriteLine("File not found.");
}
if (orders == null) return;

//1. Tính tổng số tiền của tất cả các đơn đặt hàng.
Console.WriteLine(orders.Sum(x => x.TotalAmount));
//2. Lấy ra đơn hàng có giá trị (TotalAmount) cao nhất.
orders.MaxBy(x => x.TotalAmount)?.Print();

//3. Lấy ra đơn hàng có giá trị thấp nhất.
var minTotalAmount = orders.Min(x => x.TotalAmount);
orders.MinBy(x => x.TotalAmount)?.Print();
//4. Tính giá trị trung bình của các đơn hàng.
Console.WriteLine(orders.Average(x => x.TotalAmount));

//5. Tính tổng số đơn đặt hàng của một khách hàng cụ thể.
var firstCus = orders.First().CustomerName;
Console.WriteLine("number orders of " + firstCus + "is " + orders.Where(x => x.CustomerName == firstCus).Count());

//6. Tính tổng số tiền của tất cả các đơn đặt hàng của một khách hàng cụ thể.
Console.WriteLine("Total orders of " + firstCus + "is " + orders.Where(x => x.CustomerName == firstCus).Sum( x => x.TotalAmount));

//7. Lấy ra đơn hàng có giá trị cao nhất của một khách hàng cụ thể.
Console.WriteLine("Max orders of " + firstCus + "is " + orders.Where(x => x.CustomerName == firstCus).MaxBy(x => x.TotalAmount));

//8. Lấy ra đơn hàng có giá trị thấp nhất cho một khách hàng cụ thể.
Console.WriteLine("Min orders of " + firstCus + "is " + orders.Where(x => x.CustomerName == firstCus).MinBy(x => x.TotalAmount));

//9. Tính giá trị trung bình các đơn đặt hàng của 
Console.WriteLine("Average orders of " + firstCus + "is " + orders.Where(x => x.CustomerName == firstCus).Average(x => x.TotalAmount));

public class Order
{
    public int Id { get; set; }
    public string CustomerName { get; set; }
    public float TotalAmount { get; set; }

    public Order()
    {
    }

    public Order(int id, string customerName, float totalAmount)
    {
        Id = id;
        CustomerName = customerName;
        TotalAmount = totalAmount;
    }
   public void Print()
    {
        Console.WriteLine($"ID: {this.Id}, Name: {this.CustomerName} {this.TotalAmount}");
    }
}