List<Product> products = new List<Product>
{
new Product { Id = 1, Name = "Laptop", Price = 1000 },
new Product { Id = 2, Name = "Phone", Price = 500 },
new Product { Id = 3, Name = "Headphones", Price = 50 },
new Product { Id = 4, Name = "Mouse", Price = 20 },
new Product { Id = 5, Name = "Keyboard", Price = 30 }
};
// Yêu cầu 1: Lấy ra "tên" các sản phẩm có giá trị lớn hơn 100, sắp xếp giảm dần theo giá.
products
    .Where(x => x.Price > 100)
    .OrderByDescending(x => x.Price)
    .Select(x => x.Name)
    .ToList()
    .ForEach(x => Console.WriteLine(x));
// Yêu cầu 2: Lấy ra thông tin các sản phẩm có tên chứa từ "o", sắp xếp tăng dần theo tên.
products
    .Where(x => x.Name.Contains('o'))
    .Select(x => x.Name)
    .OrderBy(x => x)
    .ToList()
    .ForEach(x => Console.WriteLine(x));
class Product
{
    public int Id { get; set; }
    public string Name { get; set; }
    public decimal Price { get; set; }
}
