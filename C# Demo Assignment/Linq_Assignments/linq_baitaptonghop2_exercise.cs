// Đọc dữ liệu từ file JSON
using Newtonsoft.Json;

var productsJson = File.ReadAllText("products.json");
var categoriesJson = File.ReadAllText("categories.json");

// Chuyển đổi JSON thành danh sách các đối tượng
var products = JsonConvert.DeserializeObject<List<Product>>(productsJson);
var categories = JsonConvert.DeserializeObject<List<Category>>(categoriesJson);
if (products == null || categories == null) return;

//Lấy danh sách tên sản phẩm và giá cho các sản phẩm có giá trị lớn hơn $100, sắp xếp theo giá giảm dần.
products
    .Where(x => x.Price > 100)
    .OrderByDescending(x => x.Price)
    .ToList()
    .ForEach(x => x.Print());
//Lấy danh sách tất cả các danh mục và số lượng sản phẩm trong mỗi danh mục, sắp xếp theo tên danh mục tăng dần.
categories.Join(
    products.GroupBy(x => x.CategoryId).Select(x => new { CategoryId = x.Key, Count = x.Count() }), // Group by categoryId + its count
    ca => ca.Id,
    pr => pr.CategoryId,
    (ca, pr) => new { CategoryName = ca.Name, Count = pr.Count }
    ).OrderBy(x => x.CategoryName).ToList();

//Lấy giá trung bình của các sản phẩm cho mỗi danh mục, sắp xếp theo tên danh mục tăng dần.
categories.Join(
    products.GroupBy(x => x.CategoryId).Select(x => new { CategoryId = x.Key, AVG = x.Average(xx => xx.Price) }),
    ca => ca.Id,
    pr => pr.CategoryId,
    (ca, pr) => new { CategoryName = ca.Name, AVG = pr.AVG }
    ).OrderBy(x => x.CategoryName).ToList();
//Lấy danh sách tên sản phẩm và giá cho 10 sản phẩm đắt nhất, sắp xếp theo giá giảm dần.
products
    .OrderBy(x => x.Price)
    .Take(10)
    .Select(x => new { ProdName = x.Name, Price = x.Price })
    .ToList()
    .ForEach(x =>
                {
                    Console.WriteLine(x.ProdName + " " + x.Price);
                }
            );

//Lấy danh sách tất cả các danh mục và giá trung bình của các sản phẩm trong mỗi danh mục, sắp xếp theo giá trung bình giảm dần.
var value1 = categories.Join(
    products.GroupBy(x => x.CategoryId).Select(x => new { CategoryId = x.Key, AVG = x.Average(xx => xx.Price) }), 
    ca => ca.Id,
    pr => pr.CategoryId,
    (ca, pr) => new { CategoryName = ca.Name, AVG = pr.AVG }
    ).OrderByDescending(x => x.AVG).ToList();
//Lấy danh sách tên sản phẩm và danh mục cho các sản phẩm có giá trị nhỏ hơn $50, sắp xếp theo tên danh mục tăng dần.
var value2 = categories
    .Join(products.Where(x => x.Price < 50).GroupBy(x => x.CategoryId).Select(x => new { CategoryId = x.Key, Prods = x.Select(xx => xx.Name) }), // Group by categoryId + its count
        ca => ca.Id,
        pr => pr.CategoryId,
        (ca, pr) => new { CategoryName = ca.Name, Prods = pr.Prods }
    ).OrderByDescending(x => x.CategoryName).ToList();

//Tính tổng giá của tất cả sản phẩm trong mỗi danh mục, sắp xếp theo tên danh mục tăng dần.
var value3 = categories
    .Join(products.GroupBy(x => x.CategoryId).Select(x => new { CategoryId = x.Key, Sum = x.Sum(xx => xx.Price) }), 
        ca => ca.Id,
        pr => pr.CategoryId,
        (ca, pr) => new { CategoryName = ca.Name, Sum = pr.Sum }
    ).OrderByDescending(x => x.CategoryName).ToList();
//Lấy danh sách tên sản phẩm và danh mục cho các sản phẩm có tên chứa từ "Apple", sắp xếp theo tên sản phẩm tăng dần. // Sai
var value4 = categories
    .Join(products.Where(x => x.Name.Contains("Apple")).GroupBy(x => x.CategoryId).Select(x => new { CategoryId = x.Key, Prods = x.Select(xx => xx.Name).ToList() }), // Group by categoryId + its count
        ca => ca.Id,
        pr => pr.CategoryId,
        (ca, pr) => new { CategoryName = ca.Name, Prods = pr.Prods }
    ).OrderBy(x => x.Prods).ToList();

//Lấy danh sách tất cả các danh mục và tổng giá của các sản phẩm trong mỗi danh mục, chỉ lấy những danh mục có tổng giá trị lớn hơn $1000.
var value5 = categories
    .Join(products.GroupBy(x => x.CategoryId).Select(x => new { CategoryId = x.Key, Sum = x.Sum(xx => xx.Price) }),
        ca => ca.Id,
        pr => pr.CategoryId,
        (ca, pr) => new { CategoryName = ca.Name, Sum = pr.Sum }
    )
    .Where(x => x.Sum > 1000)
    .ToList();
//Kiểm tra xem có danh mục nào có sản phẩm có giá trị nhỏ hơn $10 không, nếu có, lấy danh sách tên của những danh mục đó.
var value6 = categories
    .Join(products.Where(x => x.Price < 10).GroupBy(x => x.CategoryId).Select(x => new { CategoryId = x.Key, Prods = x }), // Group by categoryId + its count
        ca => ca.Id,
        pr => pr.CategoryId,
        (ca, pr) => new { CategoryName = ca.Name, Prods = pr.Prods }
    ).Where(x => x.Prods.Count() > 0).ToList();

//Lấy danh sách các sản phẩm có giá trị lớn nhất trong mỗi danh mục, sắp xếp theo tên danh mục tăng dần.
var value7 = categories
    .Join(products.GroupBy(x => x.CategoryId).Select(x => new { CategoryId = x.Key, Prods = x.MaxBy(x => x.Price) }), // Group by categoryId + its count
        ca => ca.Id,
        pr => pr.CategoryId,
        (ca, pr) => new { CategoryName = ca.Name, Prods = pr.Prods }
    )
    .OrderByDescending(x => x.CategoryName)
    .ToList();

//Lấy danh sách tất cả các danh mục và tổng số tiền của các sản phẩm trong mỗi danh mục, sắp xếp theo tổng số tiền giảm dần.
var value8 = categories
    .Join(products.GroupBy(x => x.CategoryId).Select(x => new { CategoryId = x.Key, Sum = x.Sum(xx => xx.Price) }),
        ca => ca.Id,
        pr => pr.CategoryId,
        (ca, pr) => new { CategoryName = ca.Name, Sum = pr.Sum }
    ).OrderByDescending(x => x.Sum).ToList();
//Lấy danh sách tên sản phẩm và danh mục của các sản phẩm có giá trị lớn hơn giá trị trung bình của tất cả các sản phẩm.

var value9 = categories.Join(
    products.GroupBy(x => x.CategoryId)
    
    .Select(x =>
    {
        var avgPrice = x.Average(xx => xx.Price);
        return new { CategoryId = x.Key, Prods = x.Where(xx => xx.Price>  avgPrice)};
        
    }),
    ca => ca.Id,
    pr => pr.CategoryId,
    (ca, pr) => new { CategoryName = ca.Name, AVG = pr.Prods }
    ).OrderByDescending(x => x.AVG).ToList();

//Tính tổng số tiền của tất cả các sản phẩm.
Console.WriteLine(products.Sum(x => x.Price));
//Lấy danh sách các danh mục và số lượng sản phẩm có giá trị cao nhất trong mỗi danh mục, sắp xếp theo số lượng sản phẩm giảm dần.
categories
    .Join(products.GroupBy(x => x.CategoryId)
    .Select(x => 
    {
        var maxPrice = x.Max(xx => xx.Price);

        return new { CategoryId = x.Key, ProdCount = x.Where(xx => xx.Price == maxPrice).Count() };
    }), // cho nay
        ca => ca.Id,
        pr => pr.CategoryId,
        (ca, pr) => new { CategoryName = ca.Name, ProdCount = pr.ProdCount }
    ).OrderByDescending(x => x.ProdCount).ToList();

//Lấy danh sách tên sản phẩm và danh mục của các sản phẩm có giá trị nhỏ nhất trong mỗi danh mục, sắp xếp theo giá trị giảm dần.
categories
    .Join(
    products.GroupBy(x => x.CategoryId)
    .Select(x =>
    {
        var minPrice = x.Min(xx => xx.Price);

        return new { CategoryId = x.Key, Prods = x.Where(xx => xx.Price == minPrice).Select(xx => xx.Name), MinPrice = minPrice };
    }), // cho nay
        ca => ca.Id,
        pr => pr.CategoryId,
        (ca, pr) => new { CategoryName = ca.Name, Prods = pr.Prods , MinPrice = pr.MinPrice}
    ).OrderByDescending(x => x.MinPrice).ToList();
//Lấy danh sách các danh mục có ít sản phẩm nhất.

//Lấy danh sách tên sản phẩm và danh mục của các sản phẩm có giá trị cao nhất trong mỗi danh mục, sắp xếp theo giá trị tăng dần.

//Tính tổng số tiền của các sản phẩm có giá trị lớn hơn $50.

//Lấy danh sách tên sản phẩm và danh mục của các sản phẩm có giá trị nhỏ nhất trong mỗi danh mục, sắp xếp theo giá trị tăng dần.


class Product
{
    public int Id { get; set; }
    public string Name { get; set; }
    public int CategoryId { get; set; }
    public decimal Price { get; set; }

    public void Print()
    {
        Console.WriteLine($"Id: {Id} Name: {Name} Price: {Price} Category {CategoryId} ");
    }
}

class Category
{
    public int Id { get; set; }
    public string Name { get; set; }
    public void Print()
    {
        Console.WriteLine($"Id: {Id} Name: {Name} ");
    }

}
