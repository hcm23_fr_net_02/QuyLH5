﻿using StackProgram;

namespace TestProject1
{
    public class UnitTestStackGenerics
    {
        [TestFixture]
        public class Test
        {
            [Test]
            public void Push_WhenStackEmpty_ShouldCountUp()
            {
                //Arrange 
                var stack = new MyStack<int>();
                var inputValue = 1;
                var expectedCountValue = 1;

                //Act
                stack.Push(inputValue);
                var resultCount = stack.Count;

                //Assert
                Assert.That(expectedCountValue, Is.EqualTo(resultCount));
            }

            [Test]
            public void Push_WhenStackFull_ShouldThrowException()
            {
                //Arrange 
                var stack = new MyStack<int>();
                stack.Push(1);
                stack.Push(1);
                stack.Push(1);
                stack.Push(1);
                stack.Push(1);
                stack.Push(1);
                stack.Push(1);
                stack.Push(1);
                stack.Push(1);
                stack.Push(10);

                var inputValue = 11;

                //Act & Assert
                
                Assert.Throws<Exception>(() => stack.Push(10));
            }

            [Test]
            public void Peek_WhenStackIsEmpty_ShouldThrowException()
            {
                // Arrange
                MyStack<int> stack = new MyStack<int>(); // Assuming you have a generic stack implementation

                // Act & Assert
                Assert.Throws<Exception>(() => stack.Peek());
            }

            [Test]
            public void Peek_WhenStackHasItems_ShouldReturnTopItem()
            {
                // Arrange
                MyStack<int> stack = new MyStack<int>();
                stack.Push(10);
                stack.Push(20);
                var expectedValue = 20;
                // Act
                int result = stack.Peek();

                // Assert
                Assert.That(expectedValue, Is.EqualTo(result));
            }

            [Test]
            public void Pop_WhenStackIsEmpty_ShouldThrowException()
            {
                // Arrange
                MyStack<int> stack = new MyStack<int>(); // Assuming you have a generic stack implementation

                // Act & Assert
                Assert.Throws<Exception>(() => stack.Pop());
            }

            [Test]
            public void Pop_WhenStackHasItems_ShouldReturnTopItem_AndCountDown()
            {
                // Arrange
                MyStack<int> stack = new MyStack<int>();
                stack.Push(10);
                stack.Push(20);
                int expectedCountResult = stack.Count-1;

                // Act
                int result = stack.Pop();
                int countResult = stack.Count;

                // Assert
                Assert.That(20, Is.EqualTo( result));
                Assert.That(countResult, Is.EqualTo(expectedCountResult));
            }

            [Test]
            public void IsEmpty_WhenStackEmpty_ShouldReturnTrueAndCount0()
            {
                // Arrange
                MyStack<int> stack = new MyStack<int>();

                int expectedCountResult = 0;

                // Act
                bool result = stack.IsEmpty;
                int countResult = stack.Count;

                // Assert
                Assert.IsTrue(result);
                Assert.That(countResult, Is.EqualTo(expectedCountResult));
            }

            [Test]
            public void Clear_StackEmpty_ShouldCount0()
            {
                //Arrange 
                var stack = new MyStack<int>();
                stack.Push(1);
                var expectedCountValue = 0;

                //Act
                stack.Clear();
                var resultCount = stack.Count;

                //Assert
                Assert.That(expectedCountValue, Is.EqualTo(resultCount));
            }
        }
    }
}
