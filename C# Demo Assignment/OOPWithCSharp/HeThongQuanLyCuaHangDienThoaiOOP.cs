﻿using System.Text;

class Program
{
    static void Main()
    {
        // Tạo một cửa hàng singleton
        Store myStore = Store.CreateInstance();

        // For example, u are JoneDoee
        Customer myCustomer = new Customer { Name = "John Doe", Address = "123 Main St" };

        myStore.ViewPhones();

        myStore.BuyPhone(new() { 1001, 1001, 1002, 1003 });

        Console.ReadKey();
       
    }
}

public class MobilePhone
{
    private string _name;
    public string Name 
    {
        get
        {
            return _name;
        }

        set
        {
            if (value != null && value != "")
            {
                _name = value;
            }
            else
            {
                throw new Exception("wrong Name format");

            }
        }
    }

    private string _manufacturer;
    public string Manufacturer
    {
        get
        {
            return _manufacturer;
        }

        init
        {
            if (value != null && value != "")
            {
                _manufacturer = value;
            }
            else
            {
                throw new Exception("wrong Manufacturer format");

            }
        }
    }

    private int _stockQuantity;
    public int StockQuantity
    {
        get
        {
            return _stockQuantity;
        }

        set
        {
            if (value > 0)
            {
                _stockQuantity = value;
            }
            else
            {
                throw new Exception("wrong StockQuantity format");

            }
        }
    }

    private decimal _price;
    public decimal Price
    {
        get
        {
            return _price;
        }

        set
        {
            if (value > 0)
            {
                _price = value;
            }
            else
            {
                throw new Exception("wrong Price format");

            }
        }
    }
    public int Id { get; set; }
    public MobilePhone(string name, string manufacturer, int stockQuantity, decimal price, int id)
    {
        Name = name;
        Manufacturer = manufacturer;
        StockQuantity = stockQuantity;
        Price = price;
        Id = id;
    }

    public MobilePhone(MobilePhone mobilePhone)
    {
        this.Name = mobilePhone.Name;
        this.Manufacturer = mobilePhone.Manufacturer;
        this.StockQuantity = mobilePhone.StockQuantity;
        this.Price = mobilePhone.Price;
        this.Id = mobilePhone.Id;
    }
    public void Print()
    {
        Console.WriteLine("Product Code: " + Id + " Name: " + Name + " Price: " + Price);
    }
}
public class Store
{
    private static Store? _store;
    private List<MobilePhone> _phones;
    private List<Purchase> _purchaseHistory;
    private static int _purchaseCount = 0;

    private Store()
    {
        _phones = new List<MobilePhone> { 
            new MobilePhone (  "iPhone 13",  "Apple",  10, 999.99m,  1001 ),
            new MobilePhone ("Galaxy S21", "Samsung", 8, 899.99m, 1002),
            new MobilePhone ("Pixel 6", "Google", 12, 799.99m, 1003),
            new MobilePhone ("OnePlus 9", "OnePlus", 15, 899.99m, 1004),
            new MobilePhone (  "Xperia 5 II", "Sony",  7, 899.99m,  1005 )
        };

        _purchaseHistory = new List<Purchase>();
    }

    public static Store CreateInstance()
    {
        if (_store is null)
        {
            _store = new Store();
        }
        return _store;
    }

    public void AddPhone(MobilePhone phone)
    {
        _phones.Add(phone);
    }

    public void RemovePhone(MobilePhone phone)
    {
        _phones.Remove(phone);
    }

    public void ViewPhones()
    {
        foreach(var phone in _phones)
        {
            phone.Print();
        }
    }

    public bool BuyPhone(List<int> codes)
    {
        var newPurchase = new Purchase { OrderID = _purchaseCount++, PurchaseDate = DateTime.Now };

        foreach (var i in codes)
        {
            if (_phones.FirstOrDefault(x => x.Id == i) is MobilePhone phone)
            {
                if (phone.StockQuantity > 0)
                {
                    phone.StockQuantity -= 1;
                    newPurchase.AddPhone(phone);
                }
            }
        }

        if (newPurchase.GetTotalNumberOfPhonesPurchased() > 0)
        {
            _purchaseHistory.Add(newPurchase);
            return true;
        }

        return false;
    }

    public Purchase? FindPurchaseById(int id)
    {
        return _purchaseHistory.FirstOrDefault(x => x.OrderID == id);
    }

    public void ViewAllPurchaseHistory()
    {
        foreach (var purchase in _purchaseHistory)
        {
            purchase.Print();
        }
    }
}

public class Customer
{
    private static int _count = 0;

    private string _name;
    public string Name
    {
        get
        {
            return _name;
        }

        set
        {
            if (value != null && value != "")
            {
                _name = value;
            }
            else
            {
                throw new Exception("wrong Name format");

            }
        }
    }

    private string _address;
    public string Address
    {
        get
        {
            return _address;
        }

        set
        {
            if (value != null && value != "")
            {
                _address = value;
            }
            else
            {
                throw new Exception("wrong Address format");

            }
        }
    }
    public int MemberID { get; }

    public Customer()
    {
        MemberID = _count++;
    }
    public Customer(string paraName, string paraAddress)
    {
        MemberID = _count++;
        Name = paraName;
        Address = paraAddress;
    }
}

public class Purchase
{
    public int OrderID { get; set; }
    public DateTime PurchaseDate { get; set; }
    private List<MobilePhone> _phonesPurchased;

    public Purchase()
    {
        _phonesPurchased = new List<MobilePhone>();
    }

    public void AddPhone(MobilePhone phone)
    {
        _phonesPurchased.Add(new MobilePhone(phone));
    }

    public int GetTotalNumberOfPhonesPurchased() => this._phonesPurchased.Count;

    public void ViewBorrowBooks()
    {
        foreach (MobilePhone phone in _phonesPurchased)
        {
            phone.Print();
        }
    }

    public void Print()
    {
        var s = new StringBuilder();
        s.AppendLine(this.OrderID.ToString());
        s.AppendLine(this.PurchaseDate.ToString());
        foreach (var phone in _phonesPurchased)
        {
            s.AppendLine(phone.Id.ToString() + " " + phone.Name + " " + phone.Price);
        }

    }
}



