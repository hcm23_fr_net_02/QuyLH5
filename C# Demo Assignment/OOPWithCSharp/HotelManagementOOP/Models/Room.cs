﻿using System.Text;
using System.Xml.Linq;

namespace HotelManagementOOP.Models;

public static class RoomType
{
    public const string Single = "Single";
    public const string Couple = "Couple";
    public const string Family = "Family";

    public static string PrintType()
    {
        var menu = new StringBuilder();

        menu.AppendLine("\nChoose Type:");
        menu.AppendLine($"1. {Single}");
        menu.AppendLine($"2. {Couple}");
        menu.AppendLine($"3. {Family}");

        Console.Write(menu);

        string luaChon = Console.ReadLine() ?? "";

        switch (luaChon)
        {
            case "1":
                return Single;
            case "2":
                return Couple;
            case "3":
                return Family;
            default:
                throw new Exception("Wrong RoomType");

        }
    }
}

public class Room
{
    private static int _count = 0;
    public int RoomNumber
    {
        get;
    }

    public string Type { get; set; } = RoomType.Single;

    private decimal _pricePerNight;
    public decimal PricePerNight
    {
        get
        {
            return _pricePerNight;
        }
        set
        {
            if (value > 0)
            {
                _pricePerNight = value;
            }
            else
            {
                throw new Exception("Price must > 0");
            }
        }
    }
    public bool IsAvailable { get; set; }

    private List<string> _amenities = new List<string>();
    public List<string> Amenities
    {
        get
        {
            return _amenities;
        }
        set
        {
            foreach (var a in _amenities)
            {
                if (a != "")
                {
                    _amenities.Add(a);
                }
            }
        }
    }

    public Room(string roomType, decimal pricePerNight, List<string> amenities)
    {
        RoomNumber = _count++;
        Type = roomType;
        PricePerNight = pricePerNight;
        IsAvailable = true;
        Amenities = amenities;
    }
}