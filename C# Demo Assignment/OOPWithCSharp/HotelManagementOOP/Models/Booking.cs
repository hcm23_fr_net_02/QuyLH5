﻿namespace HotelManagementOOP.Models;

public class Booking
{
    private static int _count = 0;
    public int BookingID { get; }
    public DateTime BookingDate { get; init; }
    public DateTime CheckInDate { get; set; }
    public bool IsCheckOut { get; set; }
    public List<Room> ReservedRooms { get; set; }
    public List<string> SpecialRequests { get; set; }

    public Booking(DateTime checkInDate, List<Room> reservedRooms, List<string> specialRequests)
    {
        BookingID = _count++;
        BookingDate = DateTime.Now;
        CheckInDate = checkInDate;
        ReservedRooms = reservedRooms;
        SpecialRequests = specialRequests;
    }

    public Invoice? CheckOut()
    {
        //This is just a funny example
        int totalAmount = (int)(DateTime.Now + TimeSpan.FromDays(1000) - CheckInDate).Days * (int)ReservedRooms.Sum(x => x.PricePerNight);
        Console.WriteLine($"Total of booking Id: {this.BookingID} " +
            $"is {totalAmount}" +
            $"\nDo u want to check out?" +
            $"\n1. Yes" +
            $"\n2. No");

        int choice = Int32.Parse(Console.ReadLine());

        switch (choice)
        {
            case 1:
                foreach (var room in ReservedRooms)
                {
                    room.IsAvailable = true;
                    IsCheckOut = true;
                }

                var newInvoice = new Invoice(this.BookingID, totalAmount, this.SpecialRequests);

                return newInvoice;
            default:
                return null;
        }

    }

    public void Print()
    {
        Console.WriteLine($"Id: {this.BookingID} " +
               $"BookingDate: {this.BookingDate} " +
               $"CheckInDate: {this.CheckInDate} " +
               $"Rooms: {String.Join(", ", this.ReservedRooms.Select(x => x.RoomNumber))} " +
               $"SpecialRequests: {String.Join(", ", this.SpecialRequests)} " +
               $"IsCheckOut: {this.IsCheckOut}");
    }
}
