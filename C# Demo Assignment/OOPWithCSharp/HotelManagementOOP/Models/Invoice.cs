﻿namespace HotelManagementOOP.Models;

public class Invoice
{
    public int InvoiceID { get; set; }
    public decimal TotalAmount { get; set; }
    public List<string> ServicesUsed { get; set; }

    public Invoice(int invoiceID, decimal totalAmount, List<string> servicesUsed)
    {
        InvoiceID = invoiceID;
        TotalAmount = totalAmount;
        ServicesUsed = servicesUsed;
    }
}
