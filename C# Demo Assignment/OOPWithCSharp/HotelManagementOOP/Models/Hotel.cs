﻿namespace HotelManagementOOP.Models;

public class Hotel
{
    private static Hotel? _instance;
    private List<Room> _rooms;
    private List<Booking> _bookings;
    private List<Invoice> _invoices;

    public static Hotel CreateInstance()
    {
        if (_instance == null)
            _instance = new Hotel();
        return _instance;
    }
    private Hotel()
    {
        List<string> amenities1 = new List<string> { "WiFi", "TV", "Minibar" };
        List<string> amenities3 = new List<string> { "WiFi", "TV", "Minibar", "Bồn tắm" };
        List<string> amenities4 = new List<string> { "WiFi", "TV", "Minibar", "Bồn tắm", "Phong Gym", "Studio" };
        List<string> amenities2 = new List<string> { "WiFi", "TV" };

        _rooms = new List<Room>()
        {
            new Room( RoomType.Family, 50.00m, amenities1),
            new Room( RoomType.Couple, 10.00m, amenities2),
            new Room( RoomType.Family, 12.00m, amenities2),
            new Room( RoomType.Family, 15.00m, amenities2),
            new Room( RoomType.Family, 25.00m, amenities2),
            new Room( RoomType.Couple, 60.00m, amenities3),
            new Room( RoomType.Couple, 70.00m, amenities3),
            new Room( RoomType.Couple, 80.00m, amenities3),
            new Room( RoomType.Single, 250.00m, amenities4)
        };

        _bookings = new();

        _invoices = new();
    }

    public void AddRoom(Room room)
    {
        _rooms.Add(room);
    }

    public bool RemoveRoom(int roomId)
    {
        var result = _rooms.SingleOrDefault(x => x.RoomNumber == roomId);

        if (result != null)
        {
            _rooms.Remove(result);
            return true;
        }
        return false;
    }

    public void DisplayRooms()
    {
        Console.WriteLine("Danh sách các phòng trong khách sạn:");
        foreach (var room in _rooms)
        {
            Console.WriteLine($"Phòng {room.RoomNumber}, Loại: {room.Type}, Giá tiền: {room.PricePerNight:C}, Trạng thái: {(room.IsAvailable ? "Trống" : "Đã đặt")}");
            Console.WriteLine("Tiện nghi: " + string.Join(", ", room.Amenities));
        }
    }

    public bool Book(List<int> soPhongCanDat, List<string> specialRequests, DateTime checkInDate)
    {
        var bookingRooms = new List<Room>();

        if (soPhongCanDat.Count <= 0) return false;

        foreach (var roomId in soPhongCanDat)
        {
            var findingRoom = _rooms.FirstOrDefault(x => x.RoomNumber == roomId);

            if (findingRoom == null)
            {
                throw new Exception($"Room {roomId} doesnt exist");
            }

            if (!findingRoom.IsAvailable)
            {
                throw new Exception($"Room {roomId} isn't available");
            }

            bookingRooms.Add(findingRoom);
        }

        var newBooking = new Booking(
            checkInDate,
            bookingRooms,
            specialRequests
            );

        _bookings.Add(newBooking);

        //After booking successfully, !isavailable
        bookingRooms.ForEach(x => x.IsAvailable = false);

        return true;
    }

    public void DisplayAllBookings()
    {
        foreach (var booking in _bookings)
        {
            booking.Print();
        }
    }

    public bool CheckOutByBookingId(int id)
    {
        var checkOutRoom = _bookings.FirstOrDefault(x => x.BookingID == id);
        
        if(checkOutRoom == null) return false;

        //Add bill cho chủ tiệm

        if (checkOutRoom.CheckOut() is Invoice invoice)
        {
            _invoices.Add(invoice);
            return true;
        }
        return false;
    }
}
