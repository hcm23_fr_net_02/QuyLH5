﻿namespace HotelManagementOOP.Models;

public class Customer
{
    private static int _count = 0;

    private string _name = "";
    public string Name
    {
        get
        {
            return _name;
        }
        set
        {
            if (value != "")
            {
                _name = value;
            }
        }
    }

    private string _phoneNumber = "";
    public string PhoneNumber
    {
        get
        {
            return _phoneNumber;
        }
        set
        {
            if (value != "")
            {
                _phoneNumber = value;
            }
        }
    }

    private string _address = "";
    public string Address
    {
        get
        {
            return _address;
        }
        set
        {
            if (value != "")
            {
                _address = value;
            }
        }
    }

    public int MemberID
    {
        get;
    }

    public Customer(string name, string phoneNumber, string address)
    {
        Name = name;
        PhoneNumber = phoneNumber;
        Address = address;
        MemberID = _count++;
    }
}
