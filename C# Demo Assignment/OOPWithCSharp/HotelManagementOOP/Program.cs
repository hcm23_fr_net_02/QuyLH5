﻿using HotelManagementOOP.Models;
using System.Globalization;
using System.Text;

class Program
{
    private static Hotel myHotel = Hotel.CreateInstance();

    static void Main()
    {
        Console.OutputEncoding = System.Text.Encoding.UTF8;
        bool exit = false;

        while (!exit)
        {
            try
            {
                var menu = new StringBuilder();

                menu.AppendLine("\nMenu:");
                menu.AppendLine("1. Thêm phòng");
                menu.AppendLine("2. Xóa phòng");
                menu.AppendLine("3. Hiển thị danh sách phòng");
                menu.AppendLine("4. Đặt phòng");
                menu.AppendLine("5. Hiển thị thông tin đặt phòng");
                menu.AppendLine("6. Tạo hóa đơn thanh toán");
                menu.AppendLine("7. Dọn màn hình console");
                menu.AppendLine("8. Thoát");
                menu.AppendLine("Nhập lựa chọn của bạn: ");

                Console.Write(menu);

                string luaChon = Console.ReadLine() ?? "";

                switch (luaChon)
                {
                    case "1":
                        ThemPhong();
                        break;

                    case "2":
                        XoaPhong();
                        break;

                    case "3":
                        myHotel.DisplayRooms();
                        break;

                    case "4":
                        DatPhong();
                        break;

                    case "5":
                        HienThiThongTinDatPhong();
                        break;

                    case "6":
                        ThanhToan();
                        break;

                    case "7":
                        Console.Clear();
                        break;

                    case "8":
                        exit = true;
                        break;

                    default:
                        Console.WriteLine("Lựa chọn không hợp lệ. Vui lòng thử lại.");
                        break;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }

    private static void ThanhToan()
    {
        Console.Write("Nhập số phòng cần xóa: ");
        int soPhongCanXoa = int.Parse(Console.ReadLine() ?? "0");


        if (myHotel.CheckOutByBookingId(soPhongCanXoa))
        {
            Console.WriteLine("Thanh toán thành công <3");
            return;
        }

        Console.WriteLine("Thanh toán thất bại!");
    }

    private static void HienThiThongTinDatPhong()
    {
        myHotel.DisplayAllBookings();
    }

    private static void DatPhong()
    {
        Console.Write("Nhập danh sách mã phòng cần đặt (cách nhau bởi dấu phẩy): ");
        List<string> danhSachPhong = Console.ReadLine().Split(',').ToList();

        List<int> soPhongCanDat = new();
        foreach (string i in danhSachPhong)
        {
            soPhongCanDat.Add(Int32.Parse(i));
        }

        Console.Write("Nhập các tiện nghi muốn yêu cầu (cách nhau bởi dấu phẩy): ");
        List<string> tienNghi = Console.ReadLine().Split(',').ToList();

        //Handle CheckInDate
        CultureInfo provider = CultureInfo.InvariantCulture;
        Console.Write("Nhập ngày check in (format mm/dd/yyyy): ");
        DateTime checkInDate = DateTime.ParseExact(Console.ReadLine() ?? "", "mm/dd/yyyy", provider);

        if (checkInDate < DateTime.Now)
        {
            throw new Exception("Are u trying to check in int the past?");
        }

        var result = myHotel.Book(soPhongCanDat, tienNghi, checkInDate);

        if (result == true)
        {
            Console.WriteLine($"Phòng {String.Join(", ", soPhongCanDat)} đã được đặt thành công!");
            return;
        }

        Console.WriteLine($"Không tìm thấy phòng số {soPhongCanDat}.");

    }

    private static void XoaPhong()
    {
        Console.Write("Nhập số phòng cần xóa: ");
        int soPhongCanXoa = int.Parse(Console.ReadLine() ?? "0");

        var result = myHotel.RemoveRoom(soPhongCanXoa);

        if (result == true)
        {
            Console.WriteLine($"Phòng {soPhongCanXoa} đã được xóa thành công!");
            return;
        }

        Console.WriteLine($"Không tìm thấy phòng số {soPhongCanXoa}.");

    }

    private static void ThemPhong()
    {
        Console.Write("Nhập loại phòng: ");
        string loaiPhong = RoomType.PrintType();

        Console.Write("Nhập giá tiền mỗi đêm: ");
        decimal giaTienMoiDem = decimal.Parse(Console.ReadLine() ?? "0");

        Console.Write("Nhập các tiện nghi (cách nhau bởi dấu phẩy): ");
        List<string> tienNghi = Console.ReadLine().Split(',').ToList();

        myHotel.AddRoom(new Room(loaiPhong, giaTienMoiDem, tienNghi));
        Console.WriteLine("Phòng được thêm thành công!");
    }


}
