﻿using System.Text;

public class Book
{
    private string _title;
    public string Title
    {
        get
        {
            return _title;
        }

        set
        {
            if (value != null && value != "")
            {
                _title = value;
            }
            else
            {
                throw new Exception("wrong title format");

            }
        }
    }

    private string _author;

    public string Author
    {
        get
        {
            return _author;
        }

        set
        {
            if (value != null && value != "")
            {
                _author = value;
            }
            else
            {
                throw new Exception("wrong author format");

            }
        }
    }
    private int _stockQuantity;
    public int StockQuantity
    {
        get
        {
            return _stockQuantity;
        }

        set
        {
            if (value > 0)
            {
                _stockQuantity = value;
            }
            else
            {
                throw new Exception("wrong StockQuantity format");
            }
        }
    }
    private decimal _price;
    public decimal Price
    {
        get
        {
            return _price;
        }

        set
        {
            if (value > 0)
            {
                _price = value;
            }
            else
            {
                throw new Exception("wrong Price format");
            }
        }
    }
    public int Id { get; set; }

    public void Print()
    {
        Console.WriteLine($"Id: " + Id + "Title: " + Title + "Author: " + Author + "Price: " + Price);
    }
}

public class Library
{
    private static Library? _library;
    private List<Book> _books;
    private List<Loan> _loans;
    private static int _loanCount = 0;

    private Library()
    {
        _books = new List<Book>
        {
             new Book { Title = "Book 1", Author = "Author 1", StockQuantity = 5, Price = 10.99m, Id = 1001 },
             new Book { Title = "Book 2", Author = "Author 2", StockQuantity = 3, Price = 12.99m, Id = 1002 },
             new Book { Title = "Book 3", Author = "Author 3", StockQuantity = 7, Price = 15.99m, Id = 1003 }
        };
        _loans = new List<Loan>();
    }

    public static Library CreateInstance()
    {
        if (_library == null)
        {
            _library = new Library();
        }
        return _library;
    }

    public void AddBook(Book book)
    {
        _books.Add(book);
    }

    public void RemoveBook(Book book)
    {
        _books.Remove(book);
    }

    public void DisplayBooks()
    {
        Console.WriteLine("Danh sách các cuốn sách trong thư viện:");
        foreach (var book in _books)
        {
            Console.WriteLine($"- {book.Title} ({book.Author}), Mã sách: {book.Id}, Giá: {book.Price:C}, Số lượng tồn kho: {book.StockQuantity}");
        }
    }

    public Book? FindBookByCode(int bookCode)
    {
        return _books.FirstOrDefault(book => book.Id == bookCode);
    }
    public Loan? FindLoanById(int id)
    {
        return _loans.FirstOrDefault(loan => loan.LoanID == id);
    }
    public void ViewLoans()
    {
        foreach (var loan in _loans)
        {
            loan.Print();
        }
    }

    public bool LoanBooks(List<int> codes)
    {
        var newLoan = new Loan { LoanID = _loanCount++, BorrowDate = DateTime.Now };

        foreach (var i in codes)
        {
            if (_books.FirstOrDefault(x => x.Id == i) is Book book)
            {
                if (book.StockQuantity > 0)
                {
                    book.StockQuantity -= 1;
                    newLoan.AddBook(book);
                }
            }
            else
            {
                throw new Exception("Cant find the book with Id: " + i);
            }
        }

        if (newLoan.GetTotalNumberOfBorrowBooks() > 0)
        {
            _loans.Add(newLoan);
            return true;
        }

        return false;
    }
}

public class Customer
{
    private static int _count;

    private string _name;
    public string Name
    {
        get
        {
            return _name;
        }

        init
        {
            if (value != null && value != "")
            {
                _name = value;
            }
            else
            {
                throw new Exception("wrong name format");

            }
        }
    }

    private string _address;
    public string Address
    {
        get
        {
            return _address;
        }

        set
        {
            if (value != null && value != "")
            {
                _address = value;
            }
            else
            {
                throw new Exception("wrong address format");

            }
        }
    }
    public int MemberID { get; }

    public Customer(string name, string address)
    {
        Name = name;
        Address = address;
        MemberID = _count++;
    }
}
public class Loan
{
    public int LoanID { get; set; }
    public DateTime BorrowDate { get; set; }
    public DateTime DueDate { get; set; }

    private List<Book> _borrowedBooks;

    public Loan()
    {
        _borrowedBooks = new List<Book>();
    }

    public void AddBook(Book book)
    {
        _borrowedBooks.Add(book);
    }

    public int GetTotalNumberOfBorrowBooks() => this._borrowedBooks.Count;

    public void ViewBorrowBooks()
    {
        foreach (Book book in _borrowedBooks)
        {
            book.Print();
        }
    }

    public void Print()
    {
        var s = new StringBuilder();
        s.AppendLine(this.LoanID.ToString());
        s.AppendLine(this.BorrowDate.ToString());
        s.AppendLine(this.DueDate.ToString());
        foreach (var book in _borrowedBooks)
        {
            s.AppendLine(book.Id.ToString() + " " + book.Title);
        }

    }
}

class Program
{
    static void Main()
    {
        var library = Library.CreateInstance();

        // Display the list of books in the library
        library.DisplayBooks();

        var codes = new List<int> { 1001, 1002 };

        if (library.LoanBooks(codes))
        {
            Console.WriteLine("Books loaned successfully!");
        }
        else
        {
            Console.WriteLine("Failed to loan books!");
        }

        // Display the list of books borrowed
        var loan = library.FindLoanById(0);
        if(loan == null) {
            Console.WriteLine("Not found Loan");
        }
        else
        {
            Console.WriteLine("Loan Id: " + 0);
            loan.ViewBorrowBooks();
        }
    }
}
