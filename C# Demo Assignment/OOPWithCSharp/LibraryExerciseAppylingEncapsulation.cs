﻿namespace ConsoleApp1;

internal class Program
{
    private static readonly Library _library = Library.CreateInstance();
    static void Main(string[] args)
    {


        bool exit = false;
        do
        {
            Console.InputEncoding = System.Text.Encoding.UTF8;

            try
            {
                Console.WriteLine("Menu:");
                Console.WriteLine("1. Xem danh sách các cuốn sách trong thư viện.");
                Console.WriteLine("2. Mượn sách.");
                Console.WriteLine("3. Trả sách.");
                Console.WriteLine("4. Thoát chương trình.");
                Console.Write("Chọn một lựa chọn (1-4): ");

                int choice;
                if (int.TryParse(Console.ReadLine(), out choice))
                {
                    switch (choice)
                    {
                        case 1:
                            _library.PrintBookInfo();
                            break;
                        case 2:
                            Console.WriteLine("Chọn Id sách muốn mượn");
                            int idMuon = Int32.Parse(Console.ReadLine());
                            if (!_library.Checkout(idMuon))
                            {
                                Console.WriteLine("Mượn fail");
                            }
                            break;
                        case 3:
                            Console.WriteLine("Chọn Id sách muốn mượn");
                            int idTra = Int32.Parse(Console.ReadLine());
                            if (_library.ReturnBook(idTra))
                            {
                                Console.WriteLine("Trả fail");
                            }
                            break;
                        case 4:
                            exit = true;
                            break;
                        default:
                            Console.WriteLine("Lựa chọn không hợp lệ. Vui lòng chọn lại.");
                            break;
                    }
                }
                else
                {
                    Console.WriteLine("Lựa chọn không hợp lệ. Vui lòng chọn lại.");
                }

                Console.WriteLine();
            }
            catch (Exception ex)
            {
                Console.WriteLine("System got bug: " + ex.Message);
            }
        } while (!exit);
    }

}


class Book
{
    private static int _count = 0;
    private int _id;
    private string _title;
    private string _author;
    private bool _available;


    public Book(string v1, string v2, bool v3)
    {
        _id = _count++;
        this._title = v1;
        this._author = v2;
        this._available = v3;
    }

    public int Id
    {
        get { return _id; }
    }
    public string Title
    {
        get { return _title; }
    }
    public string Author
    {
        get { return _author; }
    }
    public bool Available
    {
        get { return _available; }
        set
        {
            _available = value;
        }
    }

    public void Print()
    {
        Console.WriteLine("ID: " + this.Id + "Title: " + this.Title
            + " Author: " + this.Author
            + " Available: " + this.Available);
    }

}

class Library
{
    private static Library _instance = null!;
    private static List<Book> _books = new();
    private Library()
    {
        _books.Add(new Book("Book Title 1", "Author 1", true));
        _books.Add(new Book("Book Title 2", "Author 2", false));
        _books.Add(new Book("Book Title 3", "Author 3", true));
    }
    public static Library CreateInstance()
    {
        if (_instance == null)
        {
            _instance = new Library();
        }
        return _instance;
    }
    public bool Checkout(int id)
    {
        var book = _books.SingleOrDefault(x => x.Id == id);
        if (book is null)
        {
            throw new Exception("book doesnt exist");
        }
        if (book.Available)
        {
            book.Available = false;
            return true;
        }
        return false;
    }
    public bool ReturnBook(int id)
    {
        var book = _books.SingleOrDefault(x => x.Id == id);
        if (book is null)
        {
            throw new Exception("book doesnt exist");
        }
        if (!book.Available)
        {
            book.Available = true;
            return true;
        }
        return false;
    }

    public void PrintBookInfo()
    {
        foreach (var book in _books)
        {
            book.Print();
        }
    }
}
