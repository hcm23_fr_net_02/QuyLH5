﻿using MovieReview.Models;
using MovieReview.Service;
using System.Text;

class Program
{
    private static readonly MovieCenterService _center = MovieCenterService.CreateInstance();

    static void Main(string[] args)
    {
        while (true)
        {
            try
            {
                Console.OutputEncoding = UTF8Encoding.Unicode;
                // Hàm chính - Menu chương trình
                Console.WriteLine("=== CineWorld Movie App ===");
                Console.WriteLine("1. Xem danh sách phim");
                Console.WriteLine("2. Tìm kiếm phim");
                Console.WriteLine("3. Thêm phim mới");
                Console.WriteLine("4. Thoát");

                Console.Write("Nhập lựa chọn của bạn (1-8): ");
                string choice = Console.ReadLine() ?? "";

                switch (choice)
                {
                    case "1":
                        ViewMovieList();
                        break;
                    case "2":
                        Console.Write("Nhập từ khoá tìm kiếm: ");
                        SearchMoviesByTitle();
                        break;
                    case "3":
                        AddNewMovie();
                        break;
                    case "4":
                        ExitProgram();
                        break;
                    default:
                        Console.WriteLine("Lựa chọn không hợp lệ. Vui lòng chọn lại.");
                        break;
                }

                Console.WriteLine("-----------------------------------");
            }
            catch (Exception ex)
            {
                Console.WriteLine($"System got bug: {ex.Message}");
            }
        }
    }

    // 1: Xem toàn bộ phim trong rạp 
    static void ViewMovieList()
    {
        int itemsPerPage = 5;
        int currentPage = 1;

        while (true)
        {
            int totalPages = (int)Math.Ceiling((double)_center.TotalMovie / itemsPerPage);

            Console.Clear();
            Console.WriteLine("---- Danh sách các bộ phim ----");
            Console.WriteLine($"Trang {currentPage}/{totalPages}\n");

            var currentMoviePage = _center.GetMovies(new MovieParams(currentPage, itemsPerPage));

            foreach (var i in currentMoviePage)
            {
                Console.WriteLine($"[{i.Id}] {i.Title}");
            }

            Console.WriteLine("\n[N] Xem trang tiếp theo");
            Console.WriteLine("[P] Quay lại trang trước");
            Console.WriteLine("[B] Quay lại menu chính");
            Console.Write("Nhập ID của bộ phim để xem thông tin chi tiết (hoặc chọn N/P/B): ");

            string input = Console.ReadLine() ?? "".ToUpper();
            if (input == "N")
            {
                if (currentPage < totalPages)
                    currentPage++;
            }
            else if (input == "P")
            {
                if (currentPage > 1)
                    currentPage--;
            }
            else if (input == "B")
            {
                break;
            }
            else if (int.TryParse(input, out int movieId))
            {
                var selectedMovie = _center.GetMovieById(movieId);
                if (selectedMovie != null)
                {
                    _center.ViewMovieDetails(selectedMovie);
                }
                else
                {
                    Console.WriteLine("Không tìm thấy bộ phim với ID đã nhập.");
                    Console.WriteLine("Nhấn phím bất kỳ để tiếp tục...");
                    Console.ReadKey();
                }
            }
            else
            {
                Console.WriteLine("Lựa chọn không hợp lệ.");
                Console.WriteLine("Nhấn phím bất kỳ để tiếp tục...");
                Console.ReadKey();
            }
        }
    }

    // 2: Tìm kiếm bộ phim theo tiêu đề
    static void SearchMoviesByTitle()
    {
        Console.Clear();
        Console.WriteLine("---- Tìm kiếm bộ phim theo tiêu đề ----");
        Console.Write("Nhập tiêu đề bộ phim: ");
        string searchTitle = Console.ReadLine() ?? "";

        List<Movie> searchResults = _center.FilterMoviesByTitle(searchTitle);

        if (searchResults.Count > 0)
        {
            Console.WriteLine($"Tìm thấy {searchResults.Count} kết quả:");
            foreach (var movie in searchResults)
            {
                Console.WriteLine($"[{movie.Id}] {movie.Title}");
            }
        }
        else
        {
            Console.WriteLine("Không tìm thấy kết quả phù hợp.");
        }

        Console.WriteLine("Nhấn phím bất kỳ để tiếp tục...");
        Console.ReadKey();
    }

    // 3: Thêm bộ phim mới
    static void AddNewMovie()
    {
        Console.Clear();
        Console.WriteLine("---- Thêm bộ phim mới ----");
        Console.Write("Nhập tiêu đề: ");
        string title = Console.ReadLine() ?? "";
        Console.Write("Nhập năm sản xuất: ");
        int year = int.Parse(Console.ReadLine() ?? "");
        Console.Write("Nhập đạo diễn: ");
        string director = Console.ReadLine() ?? "";
        Console.Write("Nhập quốc gia: ");
        string country = Console.ReadLine() ?? "";

        Movie newMovie = new Movie(title, year, director, country);
        _center.AddMovie(newMovie);

        Console.WriteLine("Bộ phim đã được thêm vào cơ sở dữ liệu.");
        Console.WriteLine("Nhấn phím bất kỳ để tiếp tục...");
        Console.ReadKey();
    }

    // 4: Thoát chương trình
    static void ExitProgram()
    {
        Console.WriteLine("Chương trình đã kết thúc.");
        Environment.Exit(0);
    }
}