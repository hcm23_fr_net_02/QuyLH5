﻿namespace MovieReview.Models;

public class Review
{
    private int _rating = 0;
    private string _title = string.Empty;
    private string _content = string.Empty;

    public string Title
    {
        get => _title;
        set
        {
            if (string.IsNullOrEmpty(value))
            {
                throw new InvalidOperationException("wrong title format");
            }
            _title = value;
        }
    }
    public string Content
    {
        get => _content;
        set
        {
            if (string.IsNullOrEmpty(value))
            {
                throw new InvalidOperationException("wrong content format");
            }
            _content = value;
        }
    }
    public int Rating
    {
        get => _rating;
        set
        {
            if (value < 0 || value > 10)
            {
                throw new ArgumentOutOfRangeException("value");
            }
            _rating = value;
        }
    }

    public Review(string title, string content, int rating)
    {
        Title = title;
        Content = content;
        Rating = rating;
    }
}
