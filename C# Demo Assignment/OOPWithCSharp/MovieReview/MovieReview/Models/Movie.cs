﻿namespace MovieReview.Models;

public class Movie
{
    private static int count = 0;
    private string _title = string.Empty;
    private string _country = string.Empty;
    private string _director = string.Empty;
    private int _year;

    public int Id { get; }
    public string Title
    {
        get => _title;
        set
        {
            if (string.IsNullOrEmpty(value))
                throw new Exception("Wrong title format");
            _title = value;
        }
    }
    public int Year
    {
        get => _year;
        set
        {
            if (value > 2023 || value < 0)
                throw new Exception("Wrong year format");
            _year = value;
        }
    }
    public string Director
    {
        get => _director;
        set
        {
            if (string.IsNullOrEmpty(value))
                throw new Exception("Wrong director format");
            _director = value;
        }
    }
    public string Country
    {
        get => _country;
        set
        {
            if (string.IsNullOrEmpty(value))
                throw new Exception("Wrong country format");
            _country = value;
        }
    }
    public List<Review> Reviews
    {
        get;
    }

    public Movie(string title, int year, string director, string country)
    {
        Id = count++;
        Title = title;
        Year = year;
        Director = director;
        Country = country;
        Reviews = new List<Review>();
    }
}
