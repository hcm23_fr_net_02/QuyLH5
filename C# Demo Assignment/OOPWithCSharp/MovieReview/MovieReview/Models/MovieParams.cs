﻿namespace MovieReview.Models
{
    public record MovieParams
    (
        int PageIndex,
        int PageSize
    );
}