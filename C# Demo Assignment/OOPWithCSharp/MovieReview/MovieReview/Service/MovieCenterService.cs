﻿using MovieReview.Models;

namespace MovieReview.Service;

public class MovieCenterService
{
    private List<Movie> _movies;
    private static MovieCenterService _instance = new();

    public int TotalMovie
    {
        get => _movies.Count();
    }

    public static MovieCenterService CreateInstance()
    {
        return _instance;
    }

    private MovieCenterService()
    {
        _movies = new List<Movie>()
        {
            new Movie("Movie 1", 2000, "Director 1", "Country 1"),
            new Movie("Movie 2", 2001, "Director 2", "Country 2"),
            new Movie("Movie 3", 2002, "Director 3", "Country 3"),
            new Movie("Movie 4", 2003, "Director 4", "Country 4"),
            new Movie("Movie 5", 2004, "Director 5", "Country 5"),
            new Movie("Movie 6", 2005, "Director 6", "Country 6"),
            new Movie("Movie 7", 2006, "Director 7", "Country 7"),
            new Movie("Movie 8", 2007, "Director 8", "Country 8"),
            new Movie("Movie 9", 2008, "Director 9", "Country 9"),
            new Movie("Movie 10", 2009, "Director 10", "Country 10"),
            new Movie("Movie 11", 2010, "Director 11", "Country 11"),
            new Movie("Movie 12", 2011, "Director 12", "Country 12"),
            new Movie("Movie 13", 2012, "Director 13", "Country 13"),
            new Movie("Movie 14", 2013, "Director 14", "Country 14"),
            new Movie("Movie 15", 2014, "Director 15", "Country 15"),
            new Movie("Movie 16", 2015, "Director 16", "Country 16"),
            new Movie("Movie 17", 2016, "Director 17", "Country 17"),
            new Movie("Movie 18", 2017, "Director 18", "Country 18"),
            new Movie("Movie 19", 2018, "Director 19", "Country 19"),
            new Movie("Movie 20", 2019, "Director 20", "Country 20")
        };
    }

    public void AddMovie(Movie movie)
    {
        _movies.Add(movie);
    }

    public void DeleteMovie(Movie movie)
    {
        _movies.Remove(movie);
    }

    public Movie? GetMovieById(int movieId)
    {
        return _movies.FirstOrDefault(movie => movie.Id == movieId);
    }
    public List<Movie> GetMovies(MovieParams movieParams)
    {
        return _movies.Skip(movieParams.PageSize * (movieParams.PageIndex - 1)).Take(movieParams.PageSize).ToList();
    }

    public List<Movie> FilterMoviesByTitle(string title)
    {
        return _movies.FindAll(movie => movie.Title.ToLower().Contains(title.ToLower()));
    }

    // 1d.i: Chỉnh sửa thông tin bộ phim
    public void EditMovieDetailService(Movie movie)
    {
        Console.Clear();
        Console.WriteLine("---- Chỉnh sửa thông tin ----");
        Console.WriteLine($"ID: {movie.Id}");
        Console.WriteLine($"Tiêu đề: {movie.Title}");
        Console.WriteLine($"Năm sản xuất: {movie.Year}");
        Console.WriteLine($"Đạo diễn: {movie.Director}");
        Console.WriteLine($"Quốc gia: {movie.Country}");
        Console.WriteLine();

        Console.Write("Nhập tiêu đề mới: ");
        string newTitle = Console.ReadLine() ?? "";

        Console.Write("Nhập năm sản xuất mới: ");
        int newYear = 0;
        int.TryParse(Console.ReadLine(), out newYear);

        Console.Write("Nhập đạo diễn mới: ");
        string newDirector = Console.ReadLine() ?? "";

        Console.Write("Nhập quốc gia mới: ");
        string newCountry = Console.ReadLine() ?? "";

        movie.Title = newTitle ?? movie.Title;
        movie.Year = newYear != 0 ? newYear : movie.Year;
        movie.Director = newDirector ?? movie.Director;
        movie.Country = newCountry ?? movie.Country;

        Console.WriteLine("Thông tin bộ phim đã được cập nhật.");
        Console.WriteLine("Nhấn phím bất kỳ để tiếp tục...");
        Console.ReadKey();
    }

    // 1d.ii: Xóa bộ phim
    public void DeleteMovieService(Movie movie)
    {
        Console.Clear();
        Console.WriteLine("---- Xóa bộ phim ----");
        Console.WriteLine($"ID: {movie.Id}");
        Console.WriteLine($"Tiêu đề: {movie.Title}");
        Console.WriteLine($"Năm sản xuất: {movie.Year}");
        Console.WriteLine($"Đạo diễn: {movie.Director}");
        Console.WriteLine($"Quốc gia: {movie.Country}");
        Console.WriteLine();

        Console.Write("Bạn có chắc chắn muốn xóa bộ phim này? (Y/N): ");
        string confirm = Console.ReadLine() ?? "";

        if (confirm.ToUpper() == "Y")
        {
            DeleteMovie(movie);
            Console.WriteLine("Bộ phim đã được xóa khỏi cơ sở dữ liệu.");
        }
        else
        {
            Console.WriteLine("Hủy xóa bộ phim.");
        }

        Console.WriteLine("Nhấn phím bất kỳ để tiếp tục...");
        Console.ReadKey();
    }

    // 1d.iii: Thêm đánh giá
    public void AddReviewService(Movie movie)
    {
        Console.Clear();
        Console.WriteLine("---- Thêm đánh giá ----");
        Console.WriteLine($"ID: {movie.Id}");
        Console.WriteLine($"Tiêu đề: {movie.Title}");
        Console.WriteLine($"Năm sản xuất: {movie.Year}");
        Console.WriteLine($"Đạo diễn: {movie.Director}");
        Console.WriteLine($"Quốc gia: {movie.Country}");
        Console.WriteLine();

        Console.Write("Nhập tiêu đề đánh giá: ");
        string reviewTitle = Console.ReadLine() ?? "";

        Console.Write("Nhập nội dung đánh giá: ");
        string reviewContent = Console.ReadLine() ?? "";
        Console.Write("Nhập điểm đánh giá (từ 1 đến 10): ");

        int reviewRating = 0;
        int.TryParse(Console.ReadLine(), out reviewRating);

        Review review = new Review(reviewTitle, reviewContent, reviewRating);
        movie.Reviews.Add(review);

        Console.WriteLine("Đánh giá đã được thêm vào bộ phim.");
        Console.WriteLine("Nhấn phím bất kỳ để tiếp tục...");
        Console.ReadKey();
    }

    // 1d.iv: Xem tất cả đánh giá của bộ phim
    public void ViewAllReviewsService(Movie movie)
    {
        Console.Clear();
        Console.WriteLine("---- Tất cả đánh giá ----");
        Console.WriteLine($"ID: {movie.Id}");
        Console.WriteLine($"Tiêu đề: {movie.Title}");
        Console.WriteLine($"Năm sản xuất: {movie.Year}");
        Console.WriteLine($"Đạo diễn: {movie.Director}");
        Console.WriteLine($"Quốc gia: {movie.Country}");
        Console.WriteLine();

        if (movie.Reviews.Count > 0)
        {
            foreach (var review in movie.Reviews)
            {
                Console.WriteLine($"Tiêu đề: {review.Title}");
                Console.WriteLine($"Nội dung: {review.Content}");
                Console.WriteLine($"Điểm: {review.Rating}");
                Console.WriteLine("----------------------------");
            }
        }
        else
        {
            Console.WriteLine("Không có đánh giá nào cho bộ phim này.");
        }

        Console.WriteLine("Nhấn phím bất kỳ để tiếp tục...");
        Console.ReadKey();
    }

    public void ViewMovieDetails(Movie movie)
    {
        while (true)
        {
            Console.Clear();
            Console.WriteLine("---- Thông tin chi tiết ----");
            Console.WriteLine($"ID: {movie.Id}");
            Console.WriteLine($"Tiêu đề: {movie.Title}");
            Console.WriteLine($"Năm sản xuất: {movie.Year}");
            Console.WriteLine($"Đạo diễn: {movie.Director}");
            Console.WriteLine($"Quốc gia: {movie.Country}");
            Console.WriteLine();

            Console.WriteLine("[1] Chỉnh sửa thông tin");
            Console.WriteLine("[2] Xóa bộ phim");
            Console.WriteLine("[3] Thêm đánh giá");
            Console.WriteLine("[4] Xem tất cả đánh giá");
            Console.WriteLine("[5] Quay lại");

            Console.Write("Nhập lựa chọn của bạn: ");
            string input = Console.ReadLine() ?? "";

            switch (input)
            {
                case "1":
                    EditMovieDetailService(movie);
                    break;
                case "2":
                    DeleteMovieService(movie);
                    return;
                case "3":
                    AddReviewService(movie);
                    break;
                case "4":
                    ViewAllReviewsService(movie);
                    break;
                case "5":
                    return;
                default:
                    Console.WriteLine("Lựa chọn không hợp lệ.");
                    Console.WriteLine("Nhấn phím bất kỳ để tiếp tục...");
                    Console.ReadKey();
                    break;
            }
        }
    }
}
