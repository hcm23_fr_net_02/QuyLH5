﻿namespace ConsoleApp2;

internal class Program
{
    public static void Main(string[] args)
    {
        List<Animal> animals = new List<Animal>()
        {
            new Dog("DogA", 1,"XucXich"),
            new Dog("DogB", 1,"XucXich"),
            new Dog("DogC", 2,"BullDog"),
            new Cat("MeoA", 1,"MeoSucSinh"),
            new Cat("MeoB", 2,"MeoYummy"),
            new Cat("MeoC", 2,"MeoSimmy"),
        };


        foreach(Animal animal in animals)
        {
            animal.Speak();
        }

        foreach (Animal animal in animals)
        {
            if (animal.GetType() == typeof(Dog))
                ((Dog)animal).Bark();
            else
                ((Cat)animal).Meow();

        }
    }
}

public class Animal
{
    public string Name { get; set; }
    public int Age { get; set; }
    public string Type { get; set; }
    public string Sound { get; set; } = "Um Bo`";

    public Animal(string name, int age)
    {
        Name = name;
        Age = age;
        this.Type = this.GetType().Name;

    }

    public void Speak()
    {
        Console.WriteLine($"The {Type} named {Name} says {Sound}");
    }
}

public class Dog : Animal
{
    public string breed { get; set; } = string.Empty;
    public Dog(string name, int age, string parabreed) : base(name, age)
    {
        breed = parabreed;
        this.Sound = "Woff! Woff!";
    }
    public void Bark()
    {
        Console.WriteLine(Sound);
    }

}
public class Cat : Animal{
    public Cat(string name, int age, string paraFurColor) : base(name, age)
    {
        this.furColor = paraFurColor;
        this.Sound = "Meow! Meow!";

    }

    public string furColor { get; set; } = string.Empty;
    public void Meow()
    {
        Console.WriteLine(Sound);
    }
}
