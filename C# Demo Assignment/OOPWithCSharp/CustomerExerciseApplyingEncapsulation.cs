﻿

namespace ConsoleApp1
{
   

    class Customer
    {
        private string _name = "";
        private string _email = "";
        private decimal _accountBalance = 0;

        public string Name
        {
            get
            {
                return _name;
            }
            init
            {
                if (_name != value && value != null && value != "")
                {
                    _name = value;
                }
                else
                {
                    throw new Exception("wrong name format");

                }
            }
        }
        public string Email
        {
            get
            {
                return _email;
            }
            set
            {
                if (_email.Contains('@'))
                {
                    _email = value;
                }
                else
                {
                    throw new Exception("wrong email format");
                }
            }
        }
        public decimal AccountBalance
        {
            get
            {
                return _accountBalance;
            }
            private set
            {
                if (value < 0)
                {
                    _accountBalance = 0;
                }
                else
                {
                    _accountBalance = value;
                }
            }
        }
        public Customer(string paraName, string email, decimal paraAccountBalance)
        {
            this.Name = paraName;
            this.Email = email;
            this.AccountBalance = paraAccountBalance;
        }

        public void AddFunds(decimal amount)
        {
            if (amount > 0)
                AccountBalance += amount;
            else
            {
                throw new Exception("added amount is < 0");
            }
        }

        public void MakePurchase(decimal amount)
        {
            if (this.CanAfford(amount))
                AccountBalance -= amount;
        }

        public bool CanAfford(decimal amount)
        {
            if(AccountBalance > amount)
            {
                return true;
            }
            return false;
        }

    }
}
