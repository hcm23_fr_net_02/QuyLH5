﻿
public class DelegateDemo
{

    public delegate T MathOperation<T>(T param1, T param2);
    public T PerformOperation<T>(T param1, T param2, MathOperation<T> operation)
    {
        return operation(param1, param2);
    }

    public double Plus(double d, double e) => d + e;
    public string Concat(string d, string e) => d + " " + e;


    public int ProcessNumberReal(int d)
    {
        if (d > 0)
        {
            return d * d;
        }
        else if (d < 0)
        {
            return d * -1;
        }
        return 0;
    }


    

}
class Program
{

    static void Main()
    {
        //StringProgram.StringDemo();
        //StructProgram.StructDemo();
        //EnumProgram.EnumDemo();
        //DateTimeProgram.DateTimeDemo();
        //StackProgram.StackDemo();
        DelegateDemo delegateDemo = new DelegateDemo();
        //Console.WriteLine(delegateDemo.PerformOperation<double>(1, 2, delegateDemo.Plus));
        int[] numbers = { 1, 2, 0, -3,-9,1,-2,5,-4 };

        numbers = ProcessNumber(numbers, delegateDemo.ProcessNumberReal);
        foreach (int i in numbers)
        {
            Console.WriteLine(i);
        }
    }

    public static int[] ProcessNumber(int[] ints, Func<int, int> func)
    {
        for(int i = 0; i < ints.Length;i++)
        {
            ints[i] = func(ints[i]);
        }
        return ints;
    }
}
