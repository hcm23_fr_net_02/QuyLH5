CREATE DATABASE Assignment202
GO

USE Assignment202
GO

CREATE TABLE Trainee (
  TraineeID INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
  Full_Name VARCHAR(255) NOT NULL,
  Birth_Date DATE NOT NULL,
  Gender VARCHAR(10) NOT NULL CHECK (Gender IN ('Male', 'Female')),
  ET_IQ INT NOT NULL CHECK (ET_IQ BETWEEN 0 AND 20),
  ET_Gmath INT NOT NULL CHECK (ET_Gmath BETWEEN 0 AND 20),
  ET_English INT NOT NULL CHECK (ET_English BETWEEN 0 AND 50),
  Training_Class VARCHAR(10) NOT NULL,
  Evaluation_Notes VARCHAR(MAX) NULL
);


-- Create the tables (with the most appropriate/economic field/column constraints & types)
--	and add at least 10 records into each created table.
INSERT INTO Trainee (Full_Name, Birth_Date, Gender, ET_IQ, ET_Gmath, ET_English, Training_Class, Evaluation_Notes)
VALUES ('Nguyen Van AA', '10/03/1998', 'Male', 15, 14, 24, 'SQL', 'Sociable')
GO

INSERT INTO Trainee (Full_Name, Birth_Date, Gender, ET_IQ, ET_Gmath, ET_English, Training_Class, Evaluation_Notes)
VALUES ('Nguyen Van B', '01/20/1999', 'Male', 15, 15, 49, 'SQL', 'Energetic')
GO

INSERT INTO Trainee (Full_Name, Birth_Date, Gender, ET_IQ, ET_Gmath, ET_English, Training_Class, Evaluation_Notes)
VALUES ('Nguyen Van C', '08/14/2000', 'Male', 7, 11, 49, 'SQL', 'Polite')
GO

INSERT INTO Trainee (Full_Name, Birth_Date, Gender, ET_IQ, ET_Gmath, ET_English, Training_Class, Evaluation_Notes)
VALUES ('Nguyen Van D', '03/10/2002', 'Male', 15, 7, 40, 'SQL', 'Enthusiastic')
GO

INSERT INTO Trainee (Full_Name, Birth_Date, Gender, ET_IQ, ET_Gmath, ET_English, Training_Class, Evaluation_Notes)
VALUES ('Nguyen Van E', '02/25/2002', 'Male', 12, 7, 40, 'SQL', 'Enthusiastic')
GO

INSERT INTO Trainee (Full_Name, Birth_Date, Gender, ET_IQ, ET_Gmath, ET_English, Training_Class, Evaluation_Notes)
VALUES ('Nguyen Van F', '01/18/1998', 'Female', 2, 10, 40, 'SQL', 'Sociable')
GO

INSERT INTO Trainee (Full_Name, Birth_Date, Gender, ET_IQ, ET_Gmath, ET_English, Training_Class, Evaluation_Notes)
VALUES ('Nguyen Van G', '05/13/1998', 'Female', 7, 19, 49, 'SQL', 'Smart, Optimistic')
GO

INSERT INTO Trainee (Full_Name, Birth_Date, Gender, ET_IQ, ET_Gmath, ET_English, Training_Class, Evaluation_Notes)
VALUES ('Nguyen Van H', '1/29/1998', 'Female', 7, 7, 40, 'SQL', 'Enthusiastic')
GO

INSERT INTO Trainee (Full_Name, Birth_Date, Gender, ET_IQ, ET_Gmath, ET_English, Training_Class, Evaluation_Notes)
VALUES ('Nguyen Van I', '9/3/1998', 'Female', 10, 11, 35, 'SQL', 'Optimistic')
GO

INSERT INTO Trainee (Full_Name, Birth_Date, Gender, ET_IQ, ET_Gmath, ET_English, Training_Class, Evaluation_Notes)
VALUES ('Nguyen Thu K', '8/30/2000', 'Female', 7, 7, 30, 'SQL', 'Creative')
GO


--Change the table TRAINEE to add one more field named Fsoft_Account which is a not-null & unique field.
ALTER TABLE Trainee ADD Fsoft_Account VARCHAR(255) NOT NULL CONSTRAINT DF_TRAINEE_Fsoft_Account DEFAULT 'defaultValue'
ALTER TABLE dbo.Trainee
DROP CONSTRAINT DF_TRAINEE_Fsoft_Account
-- This converts the generated GUID to a string of length 36 characters, Then take 10 first character
UPDATE Trainee
    SET Fsoft_Account = LEFT(CONVERT(char(36), NEWID()), 10)
GO

-- ADD UNIQUE
ALTER TABLE Trainee
ADD UNIQUE (Fsoft_Account)
GO
--test
SELECT*FROM Trainee
ALTER TABLE Trainee
DROP COLUMN Fsoft_Account;
Go
--Create a VIEW that includes all the ET-passed trainees. One trainee is considered as ET-passed when he/she has the entry test points satisfied below criteria:
CREATE VIEW ET_passed AS
	SELECT *
	FROM Trainee
	WHERE ET_IQ + ET_Gmath >= 20 AND ET_IQ >= 8 AND ET_Gmath >= 8 AND ET_English >= 18
GO
--test
SELECT * FROM ET_passed;

--Query all the trainees who are passed the entry test, group them into different birth months.
SELECT	TraineeID,
	Full_Name,
	Birth_Date
FROM	ET_passed
ORDER BY MONTH(Birth_Date)



--Query the trainee who has the longest name, showing his/her age along with his/her basic information (as defined in the table).
SELECT	*
FROM	Trainee
WHERE	LEN(Full_Name) = 
	(SELECT MAX(LEN(Full_Name)) FROM Trainee) -- Subquery

--Drop Database
DROP DATABASE Assignment202
GO