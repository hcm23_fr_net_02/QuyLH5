--Query1
SELECT ProductID, Name, Color, ListPrice
FROM Production.Product;

--Query 2
SELECT ProductID, Name, Color, ListPrice
FROM Production.Product
WHERE ListPrice <> 0

--Query 3
SELECT ProductID, Name, Color, ListPrice
FROM Production.Product
WHERE Color IS NULL

--Query 4
SELECT ProductID, Name, Color, ListPrice
FROM Production.Product
WHERE Color IS NOT NULL

--Query 5
SELECT ProductID, Name, Color, ListPrice
FROM Production.Product
WHERE Color IS NOT NULL AND ListPrice > 0

--Query 6
SELECT Name + ' : ' + Color AS 'Name And Color'
FROM Production.Product
WHERE Color IS NOT NULL

--Query 7
SELECT 'Name: ' + Name + ' -- COLOR: ' + Color AS 'Name And Color'
FROM Production.Product
WHERE Color IS NOT NULL

--Query 8
SELECT ProductID, Name
FROM Production.Product
WHERE ProductID BETWEEN 400 AND 500

--Query 9
SELECT ProductID, Name, Color
FROM Production.Product
WHERE Color in ('Black', 'Blue')

--Query 10
SELECT Name, ListPrice
FROM Production.Product
WHERE Name LIKE 'S%'
ORDER BY Name

--Query 11
SELECT Name, ListPrice
FROM Production.Product
WHERE Name LIKE 'S%' OR Name LIKE 'A%'
ORDER BY Name

--Query 12
SELECT Name, ListPrice
FROM Production.Product
WHERE Name LIKE 'SPO[^K]%' 
ORDER BY Name

--Query 13
SELECT DISTINCT Color
FROM Production.Product

--Query 14
SELECT DISTINCT ProductSubcategoryID, Color
FROM Production.Product
WHERE ProductSubcategoryID IS NOT NULL AND Color IS NOT NULL
ORDER BY ProductSubcategoryID, Color DESC

--Query 15
SELECT ProductSubCategoryID
	,  LEFT([Name],35) AS [Name]
    ,  Color, ListPrice
FROM Production.Product
WHERE Color IN ('Red','Black')
      AND ProductSubCategoryID = 1
      OR ListPrice BETWEEN 1000 AND 2000
ORDER BY ProductID

--Query 16
SELECT Name, ISNULL(Color,'Unknown') AS Color, ListPrice
FROM Production.Product