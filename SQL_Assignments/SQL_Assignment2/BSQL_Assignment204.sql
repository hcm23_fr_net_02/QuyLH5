CREATE DATABASE Movie_Collection
GO
USE Movie_Collection
GO
CREATE TABLE Movie (
  Mid INT IDENTITY(1,1) PRIMARY KEY,
  Name NVARCHAR(100) NOT NULL,
  Duration INT NOT NULL CHECK (Duration >= 60),
  Genre TINYINT NOT NULL CHECK (Genre BETWEEN 1 AND 8),
  Director NVARCHAR(50),
  MoneyAtBoxOffice DECIMAL(18,2),
  Comments NVARCHAR(500)
)
GO
CREATE TABLE Actor (
  Aid INT IDENTITY(1,1) PRIMARY KEY,
  Name NVARCHAR(50) NOT NULL,
  Age TINYINT NOT NULL,
  Salary DECIMAL(18,2) NOT NULL,
  Nationality NVARCHAR(50) NOT NULL
)
GO
CREATE TABLE ActedIn (
  Mid INT FOREIGN KEY REFERENCES Movie(Mid) NOT NULL,
  Aid INT FOREIGN KEY REFERENCES Actor(Aid) NOT NULL,
  PRIMARY KEY (Mid, Aid)
)
GO

--2
ALTER TABLE Movie ADD ImageLink VARCHAR(100) NOT NULL UNIQUE
GO

INSERT INTO Movie (Name, Duration, Genre, Director, MoneyAtBoxOffice, ImageLink)
VALUES
	('Spider-Man: No Way Home', 180, 1, 'Jon Watts', 10000000000, 'linkanh1.jpg'),
	('The Batman', 175, 1, 'Matt Reeves', 770837663, 'linkanh5.jpg'),
	('Top Gun: Maverick', 131, 1, 'Joseph Kosinski', 1488737334, 'linkanh6.jpg'),
	('Doctor Strange in the Multiverse of Madness', 126, 1, 'Sam Raimi', 955775804, 'linkanh7.jpg'),
	('Jurassic World Dominion', 147, 1, 'Colin Trevorrow', 1001376910, 'linkanh8.jpg');

INSERT INTO Actor (Name, Age, Salary, Nationality)
VALUES
('Tom Holland', 26, 10000000, 'England'),
('Robert Pattinson', 36, 20000000, 'England'),
('Tom Cruise', 60, 100000000, 'United States'),
('Benedict Cumberbatch', 46, 120000000, 'England'),
('Chris Pratt', 43, 100000000, 'United States');

GO
INSERT INTO ActedIn
VALUES
(1,4),
(3,2),
(3,3),
(4,1),
(4,2),
(1,3),
(1,1),
(1,2),
(2,1),
(2,2),
(2,3)

-- Select actors older than 50 years old
SELECT *
FROM Actor
WHERE Age > 50

-- Select names and salaries of actors ordered by salary in descending order
SELECT Name, Salary
FROM Actor
ORDER BY Salary DESC

-- Select names of movies that Jon Watts 2 acted in
SELECT m.Name AS 'Name Movie'
FROM Actor ac JOIN ActedIn a ON ac.Aid = a.Aid
                JOIN Movie m ON a.Mid = m.Mid
WHERE ac.Name LIKE N'Tom Holland'

-- Select names of movies that have more than 3 actors acted in
SELECT Name, COUNT(*) AS actorIn
FROM Movie m 
INNER JOIN ActedIn ai ON m.Mid = ai.Mid
WHERE Genre = 1
GROUP BY Name
HAVING COUNT(*) > 3