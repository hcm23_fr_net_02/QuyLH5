USE AdventureWorks2008R2
GO

CREATE FUNCTION P1_Q1 ()
RETURNS TABLE
AS
RETURN
	 
	(
		SELECT PP.ProductID,PP.Name ,PP.Color, PP.ProductNumber, PP.ListPrice
		FROM Production.Product PP
		WHERE PP.ProductNumber LIKE '%CA%' AND PP.Color = 'Black'
	)
SELECT * FROM P1_Q1()

--PROBLEM 1 Q2
CREATE FUNCTION P1_Q2 (@BusinessEntityID INT)
RETURNS TABLE
AS
RETURN
(
	SELECT
		PA.AddressLine1,
        ISNULL(PA.AddressLine2,''),
        PA.City,
		PAT.Name,
        PAT.CountryRegionCode,
        PA.PostalCode
	FROM Person.Address PA
	INNER JOIN Person.BusinessEntityAddress PBEA ON PA.AddressID = PBEA.AddressID
	INNER JOIN Person.BusinessEntity PB ON PB.BusinessEntityID = PBEA.BusinessEntityID
	INNER JOIN Person.StateProvince PAT ON PA.StateProvinceID = PAT.StateProvinceID

	WHERE PB.BusinessEntityID = @BusinessEntityID
)


SELECT * FROM P1_Q2(1);

--PROBLEM 2
	--Q1
CREATE PROCEDURE P2_Q1 @ModifiedDate DATE, @UpperFlag BIT
AS
BEGIN
	IF @UpperFlag = 1
		SELECT UPPER(PP.FirstName) 
		FROM Person.Person PP
		WHERE PP.ModifiedDate > @ModifiedDate
	ELSE
		SELECT PP.FirstName
		FROM Person.Person PP
		WHERE PP.ModifiedDate > @ModifiedDate
END;

EXEC P2_Q1 '1990-09-01', 0;
EXEC P2_Q1 '1990-09-01', 1;
	--Q2
	CREATE TABLE Production.ProductInventoryAudit (

       ProductID           INT NOT NULL,
       LocationID          SMALLINT NOT NULL,
       Shelf               NVARCHAR(10) NOT NULL,
       Bin                 TINYINT NOT NULL,
       Quantity            SMALLINT NOT NULL,
       rowguid             UNIQUEIDENTIFIER NOT NULL,
       ModifiedDate        DATETIME NOT NULL,
       InsertOrDelete      CHAR(1) NOT NULL
	)

CREATE TRIGGER PROBLEM3
ON Production.ProductInventory
FOR INSERT, DELETE
AS
	BEGIN
		INSERT INTO Product.ProductInventoryAudit
		SELECT 
			ProductID,
			LocationID,
			Shelf,
			Bin,
			Quantity,
			rowguid,
			GETDATE(),  -- Capture the current datetime
			'I'         -- 'I' indicates an insertion
		FROM inserted;

		INSERT INTO Production.ProductInventoryAudit
    SELECT 
        ProductID,
        LocationID,
        Shelf,
        Bin,
        Quantity,
        rowguid,
        GETDATE(),  -- Capture the current datetime
        'D'         -- 'D' indicates a deletion
    FROM deleted;
	END;

INSERT INTO Production.Product (Name, ProductNumber, Color, StandardCost, ListPrice)
VALUES ('Sample Product', 'P1234', 'Red', 10.00, 20.00);