CREATE DATABASE ASQLASSIGNMENT202
GO

USE ASQLASSIGNMENT202
GO

CREATE TABLE [dbo].[Employee](
        [EmpNo] [int] NOT NULL,
        [EmpName] [nchar](30) COLLATE
        SQL_Latin1_General_CP1_CI_AS NOT NULL,
        [BirthDay] [datetime] NOT NULL,
        [DeptNo] [int] NOT NULL,
        [MgrNo] [int] NOT NULL,
        [StartDate] [datetime] NOT NULL,
        [Salary] [money] NOT NULL,
        [Status] [int] NOT NULL,
        [Note] [nchar](100) COLLATE
        SQL_Latin1_General_CP1_CI_AS NULL,
        [Level] [int] NOT NULL
) ON [PRIMARY]
GO

ALTER TABLE Employee
ADD CONSTRAINT PK_Emp PRIMARY KEY (EmpNo)
GO

ALTER TABLE [dbo].[Employee]  ADD  CONSTRAINT [chk_Level] CHECK  (([Level]=(7) OR [Level]=(6) OR [Level]=(5) OR [Level]=(4) OR [Level]=(3) OR [Level]=(2) OR [Level]=(1)))
GO

ALTER TABLE [dbo].[Employee]  ADD  CONSTRAINT [chk_Status] CHECK  (([Status]=(2) OR [Status]=(1) OR [Status]=(0)))
GO

ALTER TABLE [dbo].[Employee]
ADD Email NCHAR(30)
GO

ALTER TABLE [dbo].[Employee]
ADD CONSTRAINT chk_Email CHECK (Email IS NOT NULL)
GO

ALTER TABLE [dbo].[Employee] ADD CONSTRAINT chk_Email1 UNIQUE(Email)
GO

ALTER TABLE Employee
ADD CONSTRAINT DF_EmpNo DEFAULT 0 FOR EmpNo
GO

ALTER TABLE Employee
ADD CONSTRAINT DF_Status DEFAULT 0 FOR Status
GO

CREATE TABLE [dbo].[Skill](
        [SkillNo] [int] IDENTITY(1,1) NOT NULL,
        [SkillName] [nchar](30) COLLATE
         SQL_Latin1_General_CP1_CI_AS NOT NULL,
        [Note] [nchar](100) COLLATE
        SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO

ALTER TABLE Skill
ADD CONSTRAINT PK_Skill PRIMARY KEY (SkillNo)
GO

CREATE TABLE [dbo].[Department](
        [DeptNo] [int] IDENTITY(1,1) NOT NULL,
        [DeptName] [nchar](30) COLLATE
        SQL_Latin1_General_CP1_CI_AS NOT NULL,
        [Note] [nchar](100) COLLATE
        SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO

ALTER TABLE Department
ADD CONSTRAINT PK_Dept PRIMARY KEY (DeptNo)
GO

CREATE TABLE [dbo].[Emp_Skill](
        [SkillNo] [int] NOT NULL,
        [EmpNo] [int] NOT NULL,
        [SkillLevel] [int] NOT NULL,
        [RegDate] [datetime] NOT NULL,
        [Description] [nchar](100) COLLATE
        SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO

ALTER TABLE Emp_Skill
ADD CONSTRAINT PK_Emp_Skill PRIMARY KEY (SkillNo, EmpNo)
GO

ALTER TABLE Employee  ADD  CONSTRAINT [FK_1] FOREIGN KEY([DeptNo])
REFERENCES Department (DeptNo)

--Q1
INSERT INTO Department (DeptName, Note)
VALUES 
    (N'Sales', N''),
    (N'Marketing', N''),
    (N'Finance', N''),
    (N'Human Resources', N''),
    (N'IT', N''),
    (N'Operations', N''),
    (N'Research and Development', N''),
    (N'Customer Service', N'');

INSERT INTO Employee (EmpNo, EmpName, BirthDay, DeptNo, MgrNo, StartDate, Salary, Status, Note, Level, Email)
VALUES 
    (1, N'John Doe', '1990-05-15', 1, 0, '2015-01-01', 50000, 1, N'', 5, N'john.doe@example.com'),
    (2, N'Jane Smith', '1985-08-22', 2, 1, '2016-03-15', 60000, 2, N'', 6, N'jane.smith@example.com'),
    (3, N'Michael Johnson', '1988-12-10', 3, 1, '2017-02-20', 55000, 1, N'', 4, N'michael.johnson@example.com'),
    (4, N'Susan Lee', '1992-06-30', 1, 2, '2018-05-10', 65000, 2, N'', 7, N'susan.lee@example.com'),
    (5, N'David Williams', '1987-03-25', 2, 2, '2019-09-15', 70000, 1, N'', 5, N'david.williams@example.com'),
    (6, N'Laura Davis', '1995-09-18', 3, 2, '2020-12-01', 60000, 2, N'', 6, N'laura.davis@example.com'),
    (7, N'James Brown', '1989-07-12', 1, 3, '2021-04-08', 55000, 1, N'', 1, N'james.brown@example.com'),
    (8, N'Sarah Taylor', '1993-11-05', 2, 3, '2022-08-03', 65000, 2, N'', 7, N'sarah.taylor@example.com');

INSERT INTO Skill (SkillName, Note)
VALUES 
    (N'Programming', N''),
    (N'Database Management', N''),
    (N'Project Management', N''),
    (N'Communication', N''),
    (N'Problem Solving', N''),
    (N'Teamwork', N''),
    (N'Leadership', N''),
    (N'Time Management', N'');

INSERT INTO Emp_Skill (SkillNo, EmpNo, SkillLevel, RegDate, Description)
VALUES 
    (1, 1, 3, '2021-01-15', N''),
    (2, 1, 4, '2021-01-15', N''),
    (3, 2, 2, '2021-02-20', N''),
    (4, 2, 3, '2021-02-20', N''),
    (5, 3, 4, '2021-03-25', N''),
    (6, 3, 5, '2021-03-25', N''),
    (7, 4, 2, '2021-04-30', N''),
    (8, 4, 3, '2021-04-30', N''),
    (1, 5, 3, '2021-05-20', N''),
    (2, 5, 4, '2021-05-20', N''),
    (3, 6, 2, '2021-06-15', N''),
    (4, 6, 3, '2021-06-15', N''),
    (5, 7, 4, '2021-07-10', N''),
    (6, 7, 5, '2021-07-10', N''),
    (7, 8, 2, '2021-08-05', N''),
    (8, 8, 3, '2021-08-05', N'');

-- test q2
update Employee
set level = 1
select * from Employee
where EmpNo = '1'

--Q2:
CREATE PROCEDURE UpdateEmployeeLevel
AS
BEGIN
	
	UPDATE Employee
	SET Level = '2'
	WHERE YEAR(GETDATE())- YEAR(Employee.StartDate) >= 3 AND Level = '1'

	DECLARE @UpdatedRecords INT;
    SET @UpdatedRecords = @@ROWCOUNT;

	PRINT 'Number of updated records: ' + CAST(@UpdatedRecords AS NVARCHAR(10));
END;

EXEC UpdateEmployeeLevel;

--Q3
CREATE PROCEDURE Q3PROCEDURE @EmpNo INT
AS
BEGIN
	DECLARE @EmpName NVARCHAR(50);
    DECLARE @Email NVARCHAR(30);
    DECLARE @DeptName NVARCHAR(50);

	SELECT 
		@EmpName = E.EmpName,
        @Email = E.Email,
        @DeptName = D.DeptName
	FROM Employee E
	INNER JOIN Department D ON D.DeptNo = E.DeptNo
	WHERE E.EmpNo = @EmpNo;

	PRINT 'Employee Name: ' + @EmpName;
    PRINT 'Email Address: ' + @Email;
    PRINT 'Department Name: ' + @DeptName;
	
END;

DECLARE @EmployeeNumber INT;
SET @EmployeeNumber = 1; -- Replace with the desired employee's EmpNo
EXEC Q3PROCEDURE @EmpNo = @EmployeeNumber;

--Q5 Emp_Tracking
CREATE FUNCTION Emp_Tracking (@EmpNo INT)
RETURNS MONEY
AS
BEGIN
    DECLARE @Salary MONEY;

    SELECT @Salary = Salary
    FROM Employee
    WHERE EmpNo = @EmpNo;

    RETURN @Salary;
END;

SELECT dbo.Emp_Tracking(123) AS EmpSalary;

--q6
CREATE TRIGGER PreventInvalidEmployeeInfo
ON Employee
FOR INSERT, UPDATE
AS
	BEGIN
		IF EXISTS (
			SELECT 1
			FROM inserted
			WHERE Level = 1 AND Salary > 10000000
		)
		BEGIN
			PRINT('Invalid employee information: Level 1 employees cannot have salary > 10,000,000.');
			ROLLBACK TRANSACTION;
		END
	END;
