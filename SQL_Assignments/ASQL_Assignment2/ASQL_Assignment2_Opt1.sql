CREATE DATABASE ASSIGNMENT2_OPT1
GO

USE ASSIGNMENT2_OPT1
GO

CREATE TABLE Projects
(
	ProjectID INT IDENTITY(1,1) PRIMARY KEY,
	ProjectName VARCHAR(30) NOT NULL,
	ProjectStartDate DATE NOT NULL,
	ProjectDescription VARCHAR(30) NOT NULL,
	ProjectDetail VARCHAR(30) NOT NULL,
	ProjectCompletedOn DATE NOT NULL,
	MgrID INT NOT NULL,
);

CREATE TABLE Employee
(
	EmployeeID INT IDENTITY(1,1) PRIMARY KEY,
	EmployeeLastName VARCHAR(30) NOT NULL,
	EmployeeFirstName VARCHAR(30) NOT NULL,
	EmployeeHireDate DATE NOT NULL,
	EmployeeStatus BIT NOT NULL,
	SupervisorID INT,
	SocialSecurityNumber DECIMAL NOT NULL
);

CREATE TABLE Project_Modules
(
	ModuleID INT IDENTITY(1,1) PRIMARY KEY,
	EmployeeID INT NOT NULL,
	ProjectID INT NOT NULL,
	ProjectModulesDate DATE NOT NULL,
	ProjectModulesCompletedOn DATE NOT NULL,
	ProjectModulesDescription VARCHAR(30) NOT NULL,
);

CREATE TABLE Work_Done
(
	WorkDoneID INT IDENTITY(0,1) PRIMARY KEY,
	ModuleID INT NOT NULL,
	WorkDoneDate DATE NOT NULL,
	WorkDoneStatus BIT NOT NULL,
	WorkDoneDescription VARCHAR(30) NOT NULL,
);

CREATE TABLE WorkDone_Report
(
	WorkDoneID INT NOT NULL,
	EmployeeID INT NOT NULL,
)


ALTER TABLE WorkDone_Report 
ADD PRIMARY KEY (WorkDoneID) 

ALTER TABLE WorkDone_Report 
ADD FOREIGN KEY (EmployeeID) 
REFERENCES Employee (EmployeeID) 

ALTER TABLE WorkDone_Report 
ADD FOREIGN KEY (WorkDoneID) 
REFERENCES Work_Done (WorkDoneID) 

ALTER TABLE Project_Modules 
ADD FOREIGN KEY (EmployeeID) 
REFERENCES Employee (EmployeeID) 

ALTER TABLE Project_Modules 
ADD FOREIGN KEY (ProjectID) 
REFERENCES Projects (ProjectID) 


-- Insert sample records into Projects table
INSERT INTO Projects (ProjectName, ProjectStartDate, ProjectDescription, ProjectDetail, ProjectCompletedOn, MgrID)
VALUES 
	('ProjectA', '2023-09-15', 'DescriptionA', 'DetailA', '2023-12-01', 101),
	('ProjectB', '2023-10-01', 'DescriptionB', 'DetailB', '2024-01-15', 102),
	('ProjectC', '2023-09-20', 'DescriptionC', 'DetailC', '2023-12-10', 103);

-- Insert sample records into Employee table
INSERT INTO Employee (EmployeeLastName, EmployeeFirstName, EmployeeHireDate, EmployeeStatus, SupervisorID, SocialSecurityNumber)
VALUES 
	('Smith', 'John', '2022-05-15', 1, NULL, 1234567890),
	('Johnson', 'Jane', '2023-01-10', 1, 1, 2345678901),
	('Brown', 'David', '2023-03-20', 1, 1, 3456789012);

-- Insert sample records into Project_Modules table
INSERT INTO Project_Modules (EmployeeID, ProjectID, ProjectModulesDate, ProjectModulesCompletedOn, ProjectModulesDescription)
VALUES 
(3, 1, '2023-09-20', '2023-09-30', 'ModuleC'),
	(1, 1, '2023-09-20', '2023-09-30', 'ModuleA'),
	
	(2, 1, '2023-10-05', '2023-10-15', 'ModuleB');

-- Insert sample records into Work_Done table
INSERT INTO Work_Done (WorkDoneDate,ModuleID, WorkDoneStatus, WorkDoneDescription)
VALUES 
	('2023-09-25',1, 1, 'Completed task 1'),
	('2023-09-26',2, 0, 'Incomplete task 2'),
	('2023-09-27',3, 1, 'Completed task 3');
	select *from Work_Done
INSERT INTO WorkDone_Report (WorkDoneID, EmployeeID)
VALUES 
	(1, 2),
	(2, 1),
	(3, 3);

CREATE PROCEDURE Q2 
@ID INT
AS
BEGIN
	SELECT * 
	FROM Employee
	WHERE EmployeeID = @ID
END

EXEC Q2 1

CREATE FUNCTION Q3 (@ID INT)
AS
(
	SELECT WDR.EmployeeID as nguoireportdum
	FROM Work_Done WD
	INNER JOIN WorkDone_Report WDR ON WD.WorkDoneID = WDR.WorkDoneID

	WHERE PM.EmployeeID = @ID
	
)

-- Trigger for Projects table
CREATE TRIGGER Projects_InvalidInput
ON Projects
AFTER INSERT, UPDATE
AS
BEGIN
    IF EXISTS (
        SELECT 1
        FROM inserted
        WHERE ProjectStartDate > ProjectCompletedOn
    )
    BEGIN
        PRINT('Invalid input. Project start date cannot be greater than completion date.');
        ROLLBACK TRANSACTION;
        RETURN;
    END
END;

-- Trigger for Project_Modules table
CREATE TRIGGER Project_Modules_InvalidInput
ON Project_Modules
AFTER INSERT, UPDATE
AS
BEGIN
    IF EXISTS (
        SELECT 1
        FROM inserted
        WHERE ProjectModulesDate > ProjectModulesCompletedOn
    )
    BEGIN
        PRINT('Invalid input. Module start date cannot be greater than completion date.');
        ROLLBACK TRANSACTION;
        RETURN;
    END
END;
