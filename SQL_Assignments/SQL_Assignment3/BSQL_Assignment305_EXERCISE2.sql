--Q1
SELECT PCR.Name, PSP.Name 
FROM Person.CountryRegion PCR
INNER JOIN Person.StateProvince PSP ON PCR.CountryRegionCode = PSP.CountryRegionCode

--Q2
SELECT PCR.Name, PSP.Name 
FROM Person.CountryRegion PCR
INNER JOIN Person.StateProvince PSP ON PCR.CountryRegionCode = PSP.CountryRegionCode
WHERE PCR.Name LIKE 'Canada%' OR PCR.Name LIKE'Germany%' 
ORDER BY  PCR.NAME, PSP.NAME

--Q3
SELECT SSOH.SalesOrderID ,CONVERT(DATE, SSOH.OrderDate), SSOH.SalesPersonID, SSP.BusinessEntityID, SSP.SalesYTD, SSP.Bonus
FROM Sales.SalesOrderHeader SSOH
INNER JOIN Sales.SalesPerson SSP ON SSOH.SalesPersonID = SSP.BusinessEntityID

SELECT SSP.BusinessEntityID, SSP.SalesYTD, SSP.Bonus
FROM Sales.SalesPerson SSP

--Q4
SELECT SSOH.SalesOrderID ,CONVERT(DATE, SSOH.OrderDate),HRE.JobTitle, SSP.SalesYTD, SSP.Bonus
FROM Sales.SalesOrderHeader SSOH
INNER JOIN Sales.SalesPerson SSP ON SSOH.SalesPersonID = SSP.BusinessEntityID
INNER JOIN HumanResources.Employee HRE ON SSP.BusinessEntityID = HRE.BusinessEntityID

--Q5 && Q6 (BC I'VE ALREADY REMOVE EMPLOYEE TABLE FROM QUERY)
SELECT SSOH.SalesOrderID ,CONVERT(DATE, SSOH.OrderDate),PP.FirstName, PP.LastName, SSP.Bonus
FROM Sales.SalesOrderHeader SSOH
INNER JOIN Sales.SalesPerson SSP ON SSOH.SalesPersonID = SSP.BusinessEntityID
INNER JOIN Person.Person PP ON SSP.BusinessEntityID = PP.BusinessEntityID

--Q7
SELECT SSOH.SalesOrderID ,CONVERT(DATE, SSOH.OrderDate),PP.FirstName, PP.LastName
FROM Sales.SalesOrderHeader SSOH
INNER JOIN Person.Person PP ON SSOH.SalesPersonID = PP.BusinessEntityID

--Q8
SELECT SSOH.SalesOrderID ,CONVERT(DATE, SSOH.OrderDate),PP.FirstName + PP.LastName AS SALEPERSON, SSOD.ProductID, SSOD.OrderQty
FROM Sales.SalesOrderHeader SSOH
INNER JOIN Person.Person PP ON SSOH.SalesPersonID = PP.BusinessEntityID
INNER JOIN Sales.SalesOrderDetail SSOD ON SSOH.SalesOrderID = SSOD.SalesOrderID


--Q9
SELECT SSOH.SalesOrderID ,CONVERT(DATE, SSOH.OrderDate),PP.FirstName + PP.LastName AS SALEPERSON, PR_P.Name, SSOD.OrderQty
FROM Sales.SalesOrderHeader SSOH
INNER JOIN Person.Person PP ON SSOH.SalesPersonID = PP.BusinessEntityID
INNER JOIN Sales.SalesOrderDetail SSOD ON SSOH.SalesOrderID = SSOD.SalesOrderID
INNER JOIN Production.Product PR_P ON PR_P.ProductID = SSOD.ProductID

--Q9
SELECT SSOH.SalesOrderID ,CONVERT(DATE, SSOH.OrderDate),PP.FirstName + PP.LastName AS SALEPERSON, PR_P.Name, SSOD.OrderQty
FROM Sales.SalesOrderHeader SSOH
INNER JOIN Person.Person PP ON SSOH.SalesPersonID = PP.BusinessEntityID
INNER JOIN Sales.SalesOrderDetail SSOD ON SSOH.SalesOrderID = SSOD.SalesOrderID
INNER JOIN Production.Product PR_P ON PR_P.ProductID = SSOD.ProductID

--Q10
SELECT SSOH.SalesOrderID ,CONVERT(DATE, SSOH.OrderDate),PP.FirstName + PP.LastName AS SALEPERSON, PR_P.Name, SSOD.OrderQty
FROM Sales.SalesOrderHeader SSOH
INNER JOIN Person.Person PP ON SSOH.SalesPersonID = PP.BusinessEntityID
INNER JOIN Sales.SalesOrderDetail SSOD ON SSOH.SalesOrderID = SSOD.SalesOrderID
INNER JOIN Production.Product PR_P ON PR_P.ProductID = SSOD.ProductID
WHERE SSOH.SubTotal > 100000 AND YEAR(SSOH.OrderDate) = '2004'

--Q11
SELECT PCR.Name, PSP.Name 
FROM Person.CountryRegion PCR
LEFT JOIN Person.StateProvince PSP ON PCR.CountryRegionCode = PSP.CountryRegionCode

--Q12
SELECT SC.CustomerID, SSOH.SalesOrderID
FROM SALES.Customer SC
LEFT JOIN Sales.SalesOrderHeader SSOH ON SC.CustomerID = SSOH.CustomerID
WHERE SSOH.SalesOrderID IS NULL

--Q13
SELECT PP.Name, PPM.Name
FROM Production.Product PP
FULL JOIN Production.ProductModel PPM ON PP.ProductModelID = PPM.ProductModelID
