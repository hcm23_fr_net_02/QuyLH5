﻿CREATE DATABASE ASSIGNMENT302
GO

USE ASSIGNMENT302
GO

CREATE TABLE San_Pham (
    Ma_SP INT NOT NULL IDENTITY(1,1),
    Ten_SP NVARCHAR(255) NOT NULL,
    Don_Gia DECIMAL(10,2) NOT NULL,
    PRIMARY KEY (Ma_SP)
);
GO

INSERT INTO San_Pham (Ten_SP, Don_Gia)
VALUES
('iPhone 13', 10000000),
('Samsung Galaxy S23', 9000000),
('Xiaomi 13', 8000000);
GO

CREATE TABLE Khach_Hang (
    Ma_KH INT NOT NULL IDENTITY(1,1),
    Ten_KH NVARCHAR(255) NOT NULL,
    Phone_No NVARCHAR(15) NOT NULL,
    Ghi_Chu NVARCHAR(255),
    PRIMARY KEY (Ma_KH)
);
GO

INSERT INTO Khach_Hang (Ten_KH, Phone_No, Ghi_Chu)
VALUES
('Nguyễn Văn A', '0123456789', 'Khách hàng thân thiết'),
('Trần Thị B', '0987654321', 'Khách hàng mới'),
('Lê Văn C', '01234567890', 'Khách hàng vip');

GO
CREATE TABLE Don_Hang (
    Ma_DH INT NOT NULL IDENTITY(1,1),
    Ngay_DH DATETIME NOT NULL,
    Ma_SP INT NOT NULL,
    So_Luong INT NOT NULL,
    Ma_KH INT NOT NULL,
    FOREIGN KEY (Ma_SP) REFERENCES San_Pham (Ma_SP),
    FOREIGN KEY (Ma_KH) REFERENCES Khach_Hang (Ma_KH),
    PRIMARY KEY (Ma_DH)
);
GO

INSERT INTO Don_Hang (Ngay_DH, Ma_SP, So_Luong, Ma_KH)
VALUES
('2023-09-28', 1, 1, 1),
('2023-09-28', 2, 2, 2),
('2023-09-28', 3, 3, 3);
GO

CREATE VIEW Order_Slip AS
SELECT Khach_Hang.Ten_KH, Don_Hang.Ngay_DH, San_Pham.Ten_SP, Don_Hang.So_Luong, Don_Hang.So_Luong * San_Pham.Don_Gia AS Thanh_Tien
FROM Don_Hang
JOIN San_Pham ON Don_Hang.Ma_SP = San_Pham.Ma_SP
JOIN Khach_Hang ON Don_Hang.Ma_KH = Khach_Hang.Ma_KH;

SELECT * FROM Order_Slip;
