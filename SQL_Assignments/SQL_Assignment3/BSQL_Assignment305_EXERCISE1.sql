USE AdventureWorks2008R2

--Q1
SELECT P.Name
FROM Production.Product P
WHERE P.ProductSubcategoryID = (	
	SELECT PPS.ProductSubcategoryID
	FROM Production.ProductSubcategory PPS
	WHERE PPS.Name = 'Saddles'
	)

--Q2
SELECT P.Name
FROM Production.Product P
WHERE P.ProductSubcategoryID IN (	
	SELECT PPS.ProductSubcategoryID
	FROM Production.ProductSubcategory PPS
	WHERE PPS.Name LIKE 'Bo%'
	)

--Q3
SELECT P.Name
FROM Production.Product P
WHERE P.ListPrice = (
	SELECT MIN(PSUB.ListPrice)
	FROM Production.Product PSUB
	INNER JOIN Production.ProductSubcategory PS ON PS.ProductSubcategoryID = PSUB.ProductSubcategoryID
	WHERE PS.Name = 'Touring Bikes'
	)
SELECT * FROM Production.ProductSubcategory PS WHERE PS.ProductSubcategoryID = '3' 

--Q4-PART1
SELECT PCR.Name
FROM Person.CountryRegion PCR
WHERE PCR.CountryRegionCode IN (
	SELECT PSP.CountryRegionCode
	FROM Person.StateProvince PSP
	GROUP BY PSP.CountryRegionCode
	HAVING COUNT(*) < 10
)

--Q4-PART2
SELECT PCR.Name
FROM Person.CountryRegion PCR
INNER JOIN Person.StateProvince PSP ON PCR.CountryRegionCode = PSP.CountryRegionCode
GROUP BY PCR.Name, PSP.CountryRegionCode
HAVING COUNT(*) < 10

--Q5

SELECT 
    SalesPersonID,
    ISNULL(AVG(SubTotal), 0) - (SELECT AVG(SubTotal) FROM Sales.SalesOrderHeader WHERE SalesPersonID IS NOT NULL) AS SalesDiff
FROM 
    Sales.SalesOrderHeader
WHERE 
    SalesPersonID IS NOT NULL
GROUP BY 
    SalesPersonID
ORDER BY 
    SalesPersonID;

SELECT SSOH.SalesPersonID, AVG(SSOH.SubTotal) - (SELECT AVG(SSOH.SubTotal) --SUBQUERY
												FROM Sales.SalesOrderHeader SSOH
												WHERE SSOH.SalesPersonID IS NOT NULL) AS SALESDIFF
FROM Sales.SalesOrderHeader SSOH
WHERE SSOH.SalesPersonID IS NOT NULL
GROUP BY SSOH.SalesPersonID
ORDER BY 
    SalesPersonID;

SELECT AVG(SSOH.SubTotal) --SUBQUERY
FROM Sales.SalesOrderHeader SSOH
WHERE SSOH.SalesPersonID IS NOT NULL

SELECT SSOH.SalesPersonID, AVG(SSOH.SubTotal) 
FROM Sales.SalesOrderHeader SSOH
WHERE SSOH.SalesPersonID IS NOT NULL
GROUP BY SSOH.SalesPersonID

--Q6
SELECT AVG(PP.ListPrice) --SUBQUERY
FROM Production.Product PP
WHERE PP.ProductSubcategoryID IN (1,2,3)


SELECT PP.Name, PP.ListPrice - (SELECT AVG(PP.ListPrice) --SUBQUERY
								FROM Production.Product PP
								WHERE PP.ProductSubcategoryID IN (1,2,3)) AS DIFF --SUBQUERY
FROM Production.Product PP
WHERE PP.ProductSubcategoryID IN (1,2,3)


SELECT X.NAME, X.DIFF
FROM (
	SELECT PP.Name, PP.ListPrice - (SELECT AVG(PP.ListPrice) --SUBQUERY
								FROM Production.Product PP
								WHERE PP.ProductSubcategoryID IN (1,2,3)) AS DIFF --SUBQUERY
	FROM Production.Product PP
	WHERE PP.ProductSubcategoryID IN (1,2,3)
) X
WHERE  X.DIFF BETWEEN -800 AND -400
ORDER BY Diff ASC;

--Q7
SELECT P.FirstName + ' ' + P.LastName
FROM Sales.SalesPerson SP
INNER JOIN HumanResources.Employee E ON E.BusinessEntityID  = SP.BusinessEntityID
INNER JOIN Person.Person AS P ON E.BusinessEntityID = P.BusinessEntityID
WHERE Bonus > 5000

SELECT 
    (
        SELECT P.FirstName + ' ' + P.LastName
        FROM HumanResources.Employee E
        INNER JOIN Person.Person AS P ON E.BusinessEntityID = P.BusinessEntityID
        WHERE E.BusinessEntityID = SP.BusinessEntityID
    ) AS FullName
FROM Sales.SalesPerson SP
WHERE Bonus > 5000;

--Q8
	--Part1
SELECT *
FROM Sales.SalesPerson SSP
WHERE NOT EXISTS (
	SELECT*
	FROM Sales.Store SS
	WHERE SSP.BusinessEntityID = SS.SalesPersonID
)
	--Part2
SELECT SSP.BusinessEntityID
FROM Sales.SalesPerson SSP
LEFT JOIN Sales.Store SS ON SSP.BusinessEntityID = SS.SalesPersonID
WHERE SS.SalesPersonID IS NULL

--Q9
	--Part1
	SELECT PPS.ProductSubcategoryID , COUNT(*) 
	FROM Production.ProductSubcategory PPS
	GROUP BY PPS.ProductSubcategoryID
	ORDER BY PPS.ProductSubcategoryID

	--PART2
	; WITH TEMPSET (ProdSubID, COUNTEDPRODS)
	AS
	(
		SELECT ISNULL(PPS.ProductSubcategoryID, 0) AS ProductSubcategoryID, COUNT(*) 
		FROM Production.Product PP
		INNER JOIN Production.ProductSubcategory PPS ON PP.ProductSubcategoryID = PPS.ProductSubcategoryID
		
		GROUP BY PPS.ProductSubcategoryID
	)

	SELECT ISNULL(PPS_O.ProductCategoryID, 0) AS ProductCategoryID,  COUNT(TP.ProdSubID)AS SUBCAT_COUNT, SUM(TP.COUNTEDPRODS) AS SUMPRODS
	FROM Production.ProductCategory PPC_O
	INNER JOIN PRODUCTION.ProductSubcategory PPS_O ON PPC_O.ProductCategoryID = PPS_O.ProductCategoryID
	INNER JOIN TEMPSET TP ON PPS_O.ProductSubcategoryID = TP.ProdSubID
	GROUP BY PPS_O.ProductCategoryID



