CREATE DATABASE EMS
GO

USE EMS
GO

-- Create table Employee, Status = 1: are working
CREATE TABLE [dbo].[Employee](
	[EmpNo] [int] NOT NULL
,	[EmpName] [nchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
,	[BirthDay] [datetime] NOT NULL
,	[DeptNo] [int] NOT NULL
, 	[MgrNo] [int]
,	[StartDate] [datetime] NOT NULL
,	[Salary] [money] NOT NULL
,	[Status] [int] NOT NULL
,	[Note] [nchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
,	[Level] [int] NOT NULL
) ON [PRIMARY]

GO

ALTER TABLE Employee 
ADD CONSTRAINT PK_Emp PRIMARY KEY (EmpNo)
GO

ALTER TABLE [dbo].[Employee]  
ADD  CONSTRAINT [chk_Level] 
	CHECK  (([Level]=(7) OR [Level]=(6) OR [Level]=(5) OR [Level]=(4) OR [Level]=(3) OR [Level]=(2) OR [Level]=(1)))
GO
ALTER TABLE [dbo].[Employee]  
ADD  CONSTRAINT [chk_Status] 
	CHECK  (([Status]=(2) OR [Status]=(1) OR [Status]=(0)))

GO
ALTER TABLE [dbo].[Employee]
ADD Email NCHAR(30) 
GO

ALTER TABLE [dbo].[Employee]
ADD CONSTRAINT chk_Email CHECK (Email IS NOT NULL)
GO

ALTER TABLE [dbo].[Employee] 
ADD CONSTRAINT chk_Email1 UNIQUE(Email)

GO
ALTER TABLE Employee
ADD CONSTRAINT DF_EmpNo DEFAULT 0 FOR EmpNo

GO
ALTER TABLE Employee
ADD CONSTRAINT DF_Status DEFAULT 0 FOR Status

GO
CREATE TABLE [dbo].[Skill](
	[SkillNo] [int] IDENTITY(1,1) NOT NULL
,	[SkillName] [nchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
,	[Note] [nchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]

GO
ALTER TABLE Skill
ADD CONSTRAINT PK_Skill PRIMARY KEY (SkillNo)

GO
CREATE TABLE [dbo].[Department](
	[DeptNo] [int] IDENTITY(1,1) NOT NULL
,	[DeptName] [nchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
,	[Note] [nchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]

GO
ALTER TABLE Department
ADD CONSTRAINT PK_Dept PRIMARY KEY (DeptNo)

GO
CREATE TABLE [dbo].[Emp_Skill](
	[SkillNo] [int] NOT NULL
,	[EmpNo] [int] NOT NULL
,	[SkillLevel] [int] NOT NULL
,	[RegDate] [datetime] NOT NULL
,	[Description] [nchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]

GO
ALTER TABLE Emp_Skill
ADD CONSTRAINT PK_Emp_Skill PRIMARY KEY (SkillNo, EmpNo)
GO

ALTER TABLE Employee  
ADD  CONSTRAINT [FK_1] FOREIGN KEY([DeptNo])
REFERENCES Department (DeptNo)

GO
ALTER TABLE Emp_Skill
ADD CONSTRAINT [FK_2] FOREIGN KEY ([EmpNo])
REFERENCES Employee([EmpNo])

GO
ALTER TABLE Emp_Skill
ADD CONSTRAINT [FK_3] FOREIGN KEY ([SkillNo])
REFERENCES Skill([SkillNo])

GO

INSERT INTO Employee (EmpNo, EmpName, MgrNo, BirthDay, DeptNo, StartDate, Salary, Status, Email, Level) VALUES
(1,'John Doe',NULL ,'1990-01-01', 1, '2023-08-04', 100000, 1, 'john.doe@example.com', 5),
(2,'Jane Doe',1, '1991-02-02', 2, '2023-09-03', 120000, 1, 'jane.doe@example.com', 4),
(3,'Peter Parker',1, '1992-03-03', 3, '2023-10-02', 140000, 1, 'peter.parker@example.com', 3),
(4,'Mary Jane Watson',2, '1993-04-04', 4, '2023-11-01', 160000, 1, 'mary.jane.watson@example.com', 2),
(5,'Clark Kent',1, '1994-05-05', 5, '2023-12-01', 180000, 1, 'clark.kent@example.com', 1),
(6,'Lois Lane', 2,'1995-06-06', 6, '2024-01-01', 200000, 1, 'lois.lane@example.com', 6),
(7,'Bruce Wayne',4, '1996-07-07', 7, '2024-02-01', 220000, 1, 'bruce.wayne@example.com', 7),
(8,'Alfred Pennyworth',4, '1997-08-08', 8, '2024-03-01', 240000, 1, 'alfred.pennyworth@example.com', 1);
GO


INSERT INTO Skill (SkillName) VALUES
('Python'),
('Java'),
('JavaScript'),
('C++'),
('.NET'),
('SQL'),
('React'),
('Angular');
GO

INSERT INTO Department (DeptName) VALUES
('Sales'),
('Marketing'),
('Engineering'),
('Finance'),
('Human Resources'),
('Customer Support'),
('Legal'),
('IT');
GO
SELECT * FROM Skill
INSERT INTO Emp_Skill (SkillNo, EmpNo, SkillLevel, RegDate) VALUES
(1, 1, 5, '2023-08-04'),
(2, 1, 4, '2023-09-03'),
(3, 2, 4, '2023-09-03'),
(4, 3, 3, '2023-10-02'),
(5, 4, 2, '2023-11-01'),
(6, 5, 1, '2023-12-01'),
(7, 6, 6, '2024-01-01'),
(8, 7, 7, '2024-02-01')
GO
--Q2
SELECT e.EmpName, e.Email, d.DeptName
FROM Employee e
JOIN Department d ON e.DeptNo = d.DeptNo
--WHERE e.StartDate <= 
--Q3
SELECT e.EmpName
FROM Employee e
JOIN Emp_Skill es ON e.EmpNo = es.EmpNo
JOIN Skill s ON es.SkillNo = s.SkillNo
WHERE s.SkillName IN ('C++', '.NET');
--Q4
SELECT e.EmpName, e.Email, m.EmpName AS ManagerName, m.Email AS ManagerEmail
FROM Employee e
LEFT JOIN Employee m ON e.MgrNo = m.EmpNo;

--Q5
SELECT DeptName
FROM Department D
INNER JOIN Employee E ON D.DEPTNO = E.DeptNo
GROUP BY DeptName
HAVING COUNT(*) >= 2
ORDER BY DeptName DESC;

--Q6
SELECT e.EmpName, e.Email, COUNT(*) AS NumSkills
FROM Employee e
JOIN Emp_Skill es ON e.EmpNo = es.EmpNo
GROUP BY e.EmpName, e.Email
ORDER BY e.EmpName ASC;

--Q7
SELECT e.EmpName, e.Email, e.Birthday
FROM Employee e
WHERE e.EmpNo IN (SELECT es.EmpNo
                FROM Emp_Skill es
                GROUP BY es.EmpNo
                HAVING COUNT(*) > 1);

--Q8
CREATE VIEW working_employees_skills AS
	SELECT e.EmpName, s.SkillName, d.DeptName
	FROM Employee e
	JOIN Emp_Skill es ON e.EmpNo = es.EmpNo
	JOIN Skill s ON es.SkillNo = s.SkillNo
	JOIN Department d ON e.DeptNo = d.DeptNo;

sELECT * FROM working_employees_skills