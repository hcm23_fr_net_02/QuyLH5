CREATE DATABASE [DMS]
GO
USE [DMS]
GO
/****** Object:  Table [dbo].[EMPMAJOR]    Script Date: 06/01/2015 17:22:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[EMPMAJOR](
	[emp_no] [char](6) NOT NULL,
	[major] [char](3) NOT NULL,
	[major_name] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Major] PRIMARY KEY CLUSTERED 
(
	[emp_no] ASC,
	[major] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[EMP]    Script Date: 06/01/2015 17:22:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[EMP](
	[emp_no] [char](6) NOT NULL,
	[last_name] [varchar](50) NOT NULL,
	[first_name] [varchar](50) NOT NULL,
	[dept_no] [char](3) NOT NULL,
	[job] [varchar](50) NULL,
	[salary] [money] NOT NULL,
	[bonus] [money] NULL,
	[ed_level] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[emp_no] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DEPT]    Script Date: 06/01/2015 17:22:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DEPT](
	[dept_no] [char](3) NOT NULL,
	[dept_name] [varchar](50) NOT NULL,
	[mgn_no] [char](6) NULL,
	[admr_dept] [char](3) NOT NULL,
	[location] [varchar](100) NULL,
PRIMARY KEY CLUSTERED 
(
	[dept_no] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[dept_name] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[EMPPROJACT]    Script Date: 06/01/2015 17:22:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[EMPPROJACT](
	[emp_no] [char](6) NOT NULL,
	[proj_no] [char](6) NOT NULL,
	[act_no] [int] NOT NULL,
 CONSTRAINT [PK_EPA] PRIMARY KEY CLUSTERED 
(
	[emp_no] ASC,
	[proj_no] ASC,
	[act_no] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ACT]    Script Date: 06/01/2015 17:22:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ACT](
	[act_no] [int] NOT NULL,
	[act_des] [varchar](50) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[act_no] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  ForeignKey [FK_Dept]    Script Date: 06/01/2015 17:22:33 ******/
ALTER TABLE [dbo].[DEPT]  WITH CHECK ADD  CONSTRAINT [FK_Dept] FOREIGN KEY([mgn_no])
REFERENCES [dbo].[EMP] ([emp_no])
GO
ALTER TABLE [dbo].[DEPT] CHECK CONSTRAINT [FK_Dept]
GO
/****** Object:  ForeignKey [FK__EMP__dept_no__3E52440B]    Script Date: 06/01/2015 17:22:33 ******/
ALTER TABLE [dbo].[EMP]  WITH CHECK ADD FOREIGN KEY([dept_no])
REFERENCES [dbo].[DEPT] ([dept_no])
ON DELETE CASCADE
GO
/****** Object:  ForeignKey [FK_Major]    Script Date: 06/01/2015 17:22:33 ******/
ALTER TABLE [dbo].[EMPMAJOR]  WITH CHECK ADD  CONSTRAINT [FK_Major] FOREIGN KEY([emp_no])
REFERENCES [dbo].[EMP] ([emp_no])
GO
ALTER TABLE [dbo].[EMPMAJOR] CHECK CONSTRAINT [FK_Major]
GO
/****** Object:  ForeignKey [FK_EPA1]    Script Date: 06/01/2015 17:22:33 ******/
ALTER TABLE [dbo].[EMPPROJACT]  WITH CHECK ADD  CONSTRAINT [FK_EPA1] FOREIGN KEY([emp_no])
REFERENCES [dbo].[EMP] ([emp_no])
GO
ALTER TABLE [dbo].[EMPPROJACT] CHECK CONSTRAINT [FK_EPA1]
GO
/****** Object:  ForeignKey [FK_EPA2]    Script Date: 06/01/2015 17:22:33 ******/
ALTER TABLE [dbo].[EMPPROJACT]  WITH CHECK ADD  CONSTRAINT [FK_EPA2] FOREIGN KEY([act_no])
REFERENCES [dbo].[ACT] ([act_no])
GO
ALTER TABLE [dbo].[EMPPROJACT] CHECK CONSTRAINT [FK_EPA2]
GO

ALTER TABLE [dbo].[DEPT]  
NOCHECK CONSTRAINT FK_Dept 
GO  
-- Insert 8 records into the DEPT table
INSERT INTO DEPT (dept_no, dept_name, mgn_no, admr_dept, location)
VALUES
('D01', 'Sales', '10004', 'D01', 'London'),
('D02', 'Marketing', '10002', 'D02', 'New York'),
('D03', 'Engineering', '10001', 'D03', 'San Francisco'),
('D04', 'Human Resources', '10004', 'D04', 'London'),
('D05', 'Accounting', '10005', 'D05', 'London'),
('D06', 'Production', '10006', 'D06', 'London'),
('D07', 'Customer Service', '10007', 'D07', 'London'),
('D08', 'Shipping', '10008', 'D08', 'London');
GO
ALTER TABLE [dbo].[DEPT]  
CHECK CONSTRAINT FK_Dept 
GO  

INSERT INTO EMP (emp_no, last_name, first_name, dept_no, job, salary, bonus, ed_level)
VALUES
('10001', 'Smith', 'John', 'D01', 'Software Engineer', 100000, 10000, 16),
('10002', 'Jones', 'Mary', 'D02', 'Marketing Manager', 120000, 12000, 16),
('10003', 'Williams', 'William', 'D03', 'Sales Representative', 80000, 8000, 12),
('10004', 'Brown', 'David', 'D04', 'Human Resources Manager', 110000, 11000, 16),
('10005', 'Johnson', 'Michael', 'D05', 'Accounting Manager', 105000, 10500, 16),
('10006', 'Miller', 'Robert', 'D06', 'Production Manager', 95000, 9500, 14),
('10007', 'Taylor', 'Susan', 'D07', 'Customer Service Manager', 90000, 9000, 14),
('10008', 'Garcia', 'Jose', 'D08', 'Shipping Manager', 85000, 8500, 12);
GO


-- Insert 8 records into the EMPMAJOR table
INSERT INTO EMPMAJOR (emp_no, major, major_name)
VALUES
('10001', 'CS', 'Computer Science'),
('10002', 'MBA', 'Master of Business Administration'),
('10003', 'BA', 'Bachelor of Arts in Business Administration'),
('10004', 'HR', 'Human Resources'),
('10005', 'ACC', 'Accounting'),
('10006', 'ENG', 'Engineering'),
('10007', 'CS', 'Computer Science'),
('10008', 'LOG', 'Logistics');
GO
INSERT INTO ACT (act_no, act_des)
VALUES
(1, 'Gather requirements'),
(2, 'Analyze requirements'),
(3, 'Design solution'),
(4, 'Develop solution'),
(5, 'Test solution'),
(6, 'Deploy solution'),
(7, 'Maintain solution'),
(8, 'Document solution');
GO
-- Insert 8 records into the EMPPROJACT table
INSERT INTO EMPPROJACT (emp_no, proj_no, act_no)
VALUES
('10001', 'P01', 1),
('10002', 'P02', 2),
('10003', 'P03', 3),
('10004', 'P04', 4),
('10005', 'P05', 5),
('10006', 'P06', 6),
('10007', 'P07', 7),
('10008', 'P08', 8);
GO

--Q2
SELECT e.emp_no, e.last_name, e.first_name
FROM EMP e
JOIN EMPPROJACT ep ON e.emp_no = ep.emp_no
--Q3
SELECT e.emp_no, e.last_name, e.first_name
FROM EMP e
JOIN EMPMAJOR em ON e.emp_no = em.emp_no
WHERE em.major_name IN ('MAT', 'CSI')
--Q4
SELECT e.emp_no, e.last_name, e.first_name
FROM EMP e
GROUP BY E.emp_no
HAVING COUNT(*) = (
	SELECT COUNT(*) 
    FROM EMPPROJACT EP
    WHERE EP.act_no BETWEEN 90 AND 110
)
--Q5
SELECT e.emp_no, e.last_name, e.first_name, e.salary, e.dept_no,
  (SELECT AVG(salary)
   FROM EMP
   WHERE e.dept_no = dept_no) AS dept_avg_sal
FROM EMP e
--Q6
WITH AvgEducation AS (
    SELECT 
        E.emp_no,
        E.ed_level,
        E.dept_no,
        D.dept_name,
        AVG(E.ed_level) OVER (PARTITION BY E.dept_no) AS AvgDeptEducation
    FROM 
        EMP E
        JOIN DEPT D ON E.dept_no = D.dept_no
)

SELECT 
    AE.emp_no,
    AE.ed_level AS Employee_Ed_Level,
    AE.dept_name,
    AE.AvgDeptEducation AS Avg_Dept_Ed_Level
FROM 
    AvgEducation AE
WHERE 
    AE.ed_level > AE.AvgDeptEducation;

--Q7
SELECT TOP 1 
    D.dept_no,
    D.dept_name,
    SUM(E.salary + ISNULL(E.bonus, 0)) AS TotalPayroll
FROM 
    DEPT D
    JOIN EMP E ON D.dept_no = E.dept_no
GROUP BY 
    D.dept_no, D.dept_name
ORDER BY 
    TotalPayroll DESC;
--Q8
SELECT TOP 5 *
FROM EMP
ORDER BY salary DESC;
