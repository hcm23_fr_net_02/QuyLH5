CREATE DATABASE ASSIGNMENT304
GO

USE ASSIGNMENT304
GO

CREATE TABLE DEPARTMENT (
    DeptNo INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
    DeptName NVARCHAR(255) NOT NULL,
    Note NVARCHAR(255)
);

CREATE TABLE EMPLOYEE (
    EmpNo INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
    EmpName NVARCHAR(255) NOT NULL,
    BirthDay DATETIME NOT NULL,
    Email NVARCHAR(255) NOT NULL UNIQUE,
    DeptNo INT NOT NULL,
    MgrNo INT NOT NULL DEFAULT 0,
    StartDate DATETIME NOT NULL,
    Salary MONEY NOT NULL,
    Level INT NOT NULL CHECK (Level BETWEEN 1 AND 7),
    Status INT NOT NULL DEFAULT 0,
    Note NVARCHAR(255)
);
CREATE TABLE SKILL (
    SkillNo INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
    SkillName NVARCHAR(255) NOT NULL,
    Note NVARCHAR(255)
);
CREATE TABLE EMP_SKILL (
    SkillNo INT NOT NULL FOREIGN KEY REFERENCES SKILL (SkillNo),
    EmpNo INT NOT NULL FOREIGN KEY REFERENCES EMPLOYEE (EmpNo),
    SkillLevel INT NOT NULL CHECK (SkillLevel BETWEEN 1 AND 3),
    RegDate DATETIME NOT NULL,
    Description NVARCHAR(255),
    PRIMARY KEY (SkillNo, EmpNo)
);



-- EMPLOYEE table
INSERT INTO EMPLOYEE (EmpName, BirthDay, Email, DeptNo, StartDate, Salary, Level, Status, Note)
VALUES
('John Doe', '1980-01-01', 'john.doe@example.com', 1, '2023-09-28', 10000000, 7, 0, NULL),
('Jane Doe', '1981-02-02', 'jane.doe@example.com', 2, '2023-09-28', 11000000, 6, 0, NULL),
('Peter Parker', '1982-03-03', 'peter.parker@example.com', 3, '2023-09-28', 12000000, 5, 0, NULL),
('Mary Jane Watson', '1983-04-04', 'mary.jane.watson@example.com', 4, '2023-09-28', 13000000, 4, 0, NULL),
('Clark Kent', '1984-05-05', 'clark.kent@example.com', 5, '2023-09-28', 14000000, 3, 0, NULL),
('Lois Lane', '1985-06-06', 'lois.lane@example.com', 6, '2023-09-28', 15000000, 2, 0, NULL),
('Bruce Wayne', '1986-07-07', 'bruce.wayne@example.com', 7, '2023-09-28', 16000000, 1, 0, NULL),
('Alfred Pennyworth', '1987-08-08', 'alfred.pennyworth@example.com', 8, '2023-09-28', 17000000, 1, 0, NULL);

-- SKILL table
INSERT INTO SKILL (SkillName, Note)
VALUES
('Python', NULL),
('Java', NULL),
('JavaScript', NULL),
('C++', NULL),
('.NET', NULL),
('SQL', NULL),
('React', NULL),
('Angular', NULL),
('Node.js', NULL),
('Docker', NULL),
('Kubernetes', NULL),
('AWS', NULL),
('Azure', NULL),
('GCP', NULL),
('DevOps', NULL);

-- DEPARTMENT table
INSERT INTO DEPARTMENT (DeptName, Note)
VALUES
('Sales', NULL),
('Marketing', NULL),
('Engineering', NULL),
('Finance', NULL),
('Human Resources', NULL),
('Customer Support', NULL),
('Legal', NULL),
('IT', NULL);
SELECT * FROM SKILL
INSERT INTO EMP_SKILL (SkillNo, EmpNo, SkillLevel, RegDate, Description)
VALUES
(2, 1, 1, '2023-09-28', NULL),
(3, 1, 2, '2023-09-28', NULL),
(4, 2, 3, '2023-09-28', NULL),
(5, 2, 1, '2023-09-28', NULL),
(6, 3, 2, '2023-09-28', NULL),
(7, 3, 3, '2023-09-28', NULL),
(8, 4, 1, '2023-09-28', NULL),
(9, 4, 2, '2023-09-28', NULL),
(10, 5,3, '2023-09-28', NULL),
(11, 5, 1, '2023-09-28', NULL),
(12, 6, 2, '2023-09-28', NULL),
(13, 7, 3, '2023-09-28', NULL),
(14, 7, 1, '2023-09-28', NULL),
(15, 8, 2, '2023-09-28', NULL),
(14, 8, 3, '2023-09-28', NULL),
(1, 1, 3, '2023-09-28', NULL);

--Q3
SELECT e.EmpName
FROM Employee e
JOIN Emp_Skill es ON e.EmpNo = es.EmpNo
JOIN Skill s ON es.SkillNo = s.SkillNo
WHERE s.SkillName IN ('C++', '.NET');

--Q4
SELECT e.EmpName, m.EmpName AS ManagerName, m.Email AS ManagerEmail
FROM Employee e
LEFT JOIN Employee m ON e.MgrNo = m.EmpNo;

--Q5
SELECT DeptName
FROM Department D
INNER JOIN Employee E ON D.DEPTNO = E.DeptNo
GROUP BY DeptName
HAVING COUNT(*) >= 2
ORDER BY DeptName DESC;

--Q6
SELECT e.EmpName, e.Email, COUNT(*) AS NumSkills
FROM Employee e
JOIN Emp_Skill es ON e.EmpNo = es.EmpNo
GROUP BY e.EmpName, e.Email
ORDER BY e.EmpName ASC;

--Q7
SELECT e.EmpName, e.Email, e.Birthday
FROM Employee e
WHERE e.EmpNo IN (SELECT es.EmpNo
                FROM Emp_Skill es
                GROUP BY es.EmpNo
                HAVING COUNT(*) > 1);

--Q8
CREATE VIEW working_employees_skills AS
	SELECT e.EmpName, s.SkillName, d.DeptName
	FROM Employee e
	JOIN Emp_Skill es ON e.EmpNo = es.EmpNo
	JOIN Skill s ON es.SkillNo = s.SkillNo
	JOIN Department d ON e.DeptNo = d.DeptNo;
GO
SELECT * FROM working_employees_skills