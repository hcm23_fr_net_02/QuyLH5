﻿

--Câu 1: In ra danh sách các sản phẩm(MaSP, TenSP) do "Viet Nam" sản xuất 
--			hoặc các sản phẩm bán trong ngày 23/052023
--Câu 2: In ra danh sách các sản phẩm (MaSP, TenSP) không bán được
--Câu 3: Tìm số hóa đon đã mua tất cả các sản phẩm do Viet Nam sản xuất

CREATE DATABASE TEST
GO

USE TEST
GO

CREATE TABLE KhachHang(
	MaKH INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
	HoTen NVARCHAR(30) NOT NULL,
	DiaChi NVARCHAR(120) NOT NULL,
	SoDT NVARCHAR(10) NOT NULL,
	NgSinh DATE NOT NULL,
	Doanhso MONEY NOT NULL,
	NgDk DATE NOT NULL,
);

CREATE TABLE NhanVien(
	MaNV INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
	HoTen NVARCHAR(30) NOT NULL,
	SoDT NVARCHAR(10) NOT NULL,
	NgVaoLam DATE NOT NULL,
);

CREATE TABLE SanPham(
	MaSP INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
	TenSP NVARCHAR(30) NOT NULL,
	DVT CHAR(5) NOT NULL,
	NuocSX NVARCHAR(20) NOT NULL,
	Gia MONEY NOT NULL,
);

CREATE TABLE HoaDon( -- SoHD, NgHD, MaKH, MaNV, TriGia
	SoHD INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
	MaNV INT NOT NULL, -- fk
	MaKH INT NOT NULL, --fk
	TriGia MONEY NOT NULL,
	NgHD DATE NOT NULL,
);

CREATE TABLE CTHD( 
	SoHD INT NOT NULL, --fk
	MaSP INT NOT NULL, -- fk
	SL CHAR(5) NOT NULL, 
);

ALTER TABLE HoaDon  ADD  CONSTRAINT FK_1 FOREIGN KEY(MaNV)
REFERENCES NhanVien (MaNV)

ALTER TABLE HoaDon  ADD  CONSTRAINT [FK_2] FOREIGN KEY(MaKH)
REFERENCES KhachHang (MaKH)

ALTER TABLE CTHD
ADD CONSTRAINT PK_CTHD PRIMARY KEY (SoHD, MaSP)
GO

ALTER TABLE CTHD  ADD  CONSTRAINT [FK_3] FOREIGN KEY(SoHD)
REFERENCES HoaDon (SoHD)

ALTER TABLE CTHD  ADD  CONSTRAINT [FK_4] FOREIGN KEY(MaSP)
REFERENCES SanPham (MaSP)
/*
	KhachHang(MaKH, HoTen, DiaChi, SoDT, NgSinh, Doanhso, NgDk)
	NhanVien(MaNV, HoTen, SoDT, NgVaoLam)
	SanPham(MaSP, TenSP, DVT, NuocSX, Gia)
	HoaDon(SoHD, NgHD, MaKH, MaNV, TriGia)
	CTHD(SoHD, MaSP, SL) 
*/
--INSERT
INSERT INTO KhachHang(HoTen, DiaChi, SoDT, NgSinh, Doanhso, NgDk) 
VALUES 
	('A', 'AADDRESSAAA', '11', '1/1/2000', '100', '1/1/2018' ),
	('b', 'BADDRESSBBB', '12', '1/1/2000', '100', '3/1/2018' ),
	('C', 'CADDRESSCCC', '13', '1/1/2000', '100', '2/1/2018' );

INSERT INTO NhanVien(HoTen, SoDT, NgVaoLam) 
VALUES 
	('AMV', '11', '3/1/2017'),
	('bNV', '12', '1/3/2017'),
	('CNV', '13', '1/1/2017');

INSERT INTO SanPham( TenSP, DVT, NuocSX, Gia) 
VALUES 
	('AsANPHAM', 'VND','Viet Nam','11'),
	('bAsANPHAM','VND','Viet Nam','12'),
	('CAsANPHAM','VND','Viet Nam' ,'13');

INSERT INTO HoaDon (NgHD, MaKH, MaNV, TriGia)
VALUES
	('1/1/2020','1', '1','15'),
	('1/1/2020','3', '2','50'),
	('1/1/2019','2', '1','35');
GO
INSERT INTO CTHD(SoHD, MaSP, SL)
VALUES
	('1', '3','15'),
	('1', '1','15'),
	('1', '2','5'),
	('2', '1','35'),
	('2', '2','15'),
	('3', '3','5'),
	('2', '3','35');


	--Câu 1: In ra danh sách các sản phẩm(MaSP, TenSP) do "Viet Nam" sản xuất 
--			hoặc các sản phẩm bán trong ngày 23/052023
SELECT SP.MaSP, SP.TenSP
FROM SANPHAM SP
INNER JOIN CTHD C ON C.MaSP = SP.MaSP
INNER JOIN HOADON HD ON HD.SoHD = C.SoHD
WHERE SP.NuocSX = 'Viet Nam' OR HD.NgHD = '3/05/2023'

--Câu 2: In ra danh sách các sản phẩm (MaSP, TenSP) không bán được
SELECT SP.MaSP, SP.TenSP
FROM SANPHAM SP
INNER JOIN CTHD C ON C.MaSP = SP.MaSP
WHERE C.MaSP IS NULL

--Câu 3: Tìm số hóa đon đã mua tất cả các sản phẩm do Viet Nam sản xuất

SELECT HD.SoHD
FROM HOADON HD
INNER JOIN CTHD C ON C.SoHD = HD.SoHD
INNER JOIN SanPham SP ON SP.MaSP = C.MaSP
WHERE SP.NuocSX = 'VIETNAM'
GROUP BY HD.SoHD
HAVING COUNT(*) = (
	SELECT COUNT(*)
	FROM SANPHAM SP
	WHERE SP.NuocSX = 'VIETNAM'
	
)



